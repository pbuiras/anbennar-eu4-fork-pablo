tuathak1_missions = {
	slot = 1 
	generic = no
	ai = yes 
	potential = {
		primary_culture = tuathak
	}
	
	
}
tuathak2_missions = {
	slot = 2
	generic = no
	ai = yes 
	potential = {
		primary_culture = tuathak
	}
	
	tuathak_iadth_transgressors = {
		icon = mission_scandinavian_port_authority
		required_missions = { tuathak_rally_the_guard } 
		generic = no
		position = 2
		
		trigger = {
			2050 = { owned_by = ROOT }
			2017 = { owned_by = ROOT }
			2016 = { owned_by = ROOT }
			2014 = { owned_by = ROOT }
			2003 = { owned_by = ROOT }
			NOT = { exists = H09 }
		}
		effect = {
			every_known_country = {
				limit = {
					primary_culture = tuathak
				}
				add_war_exhaustion = -2
				add_mil_power = 25
			}
			add_war_exhaustion = -2
			add_mil_power = 25
		}
	}
}
tuathak3_missions = {
	slot = 3
	generic = no
	ai = yes 
	potential = {
		primary_culture = tuathak
	}
	
	tuathak_rally_the_guard = {
		icon = mission_scandinavian_port_authority
		required_missions = {  } 
		generic = no
		position = 1
		
		trigger = {
			army_size_percentage = 1
		}
		effect = {
			add_country_modifier = {
				name = "thriving_arms_industry"
				duration = 9125 
			}
		}
	}
	tuathak_golden_acres = {
		icon = mission_scandinavian_port_authority
		required_missions = { tuathak_rally_the_guard } 
		generic = no
		position = 2
		
		trigger = {
			capital_scope = {
				area_for_scope_province = {
						type = all
						owned_by = ROOT 
				}
			}
			prestige = 30
		}
		effect = {
			capital_scope = {
				add_base_manpower = 1
				area = {
					limit = { owned_by = ROOT }
					add_province_modifier = {
						name = "tuathak_glade_of_warriors"
						duration = 9125 
					}
				}
			}
		}
	}
}
tuathak4_missions = {
	slot = 4
	generic = no
	ai = yes 
	potential = {
		primary_culture = tuathak
	}
	
	tuathak_aftermath_of_the_randrunnse_conquest = {
		icon = mission_scandinavian_port_authority
		required_missions = {  } 
		generic = no
		position = 1
		
		trigger = {
			any_province = {
				is_city = yes 
				has_port = yes
				region = randrunnse_region
				development = 14
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			random_province = {
				limit = {
					is_city = yes 
					has_port = yes
					region = randrunnse_region
					development = 13
					country_or_non_sovereign_subject_holds = ROOT
				}
				add_base_manpower = 1
			}
			add_adm_power = 25
			add_dip_power = 25
			add_mil_power = 25
		}
	}
	tuathak_resume_the_hibernal_crusade= {
		icon = mission_scandinavian_port_authority
		required_missions = { 
			tuathak_rally_the_guard
			tuathak_aftermath_of_the_randrunnse_conquest 
		} 
		generic = no
		position = 2
		
		
		trigger = {
			any_known_country = {
				primary_culture = snecboth
				OR = {
					is_in_war = {
						attackers = ROOT
						defenders = THIS
					}
					is_in_war = {
						attackers = THIS
						defenders = ROOT
					}
				}
			}
		}
		effect = {
			add_country_modifier = { name = "tuathak_driven_to_end_the_winter" duration = 9125 }
		}
	}
	tuathak_tame_the_savage_north = {
		icon = mission_scandinavian_port_authority
		required_missions = { tuathak_resume_the_hibernal_crusade } 
		generic = no
		position = 3
		
		trigger = {
			OR = {
				AND = {
					1112 = { owned_by = ROOT }
					NOT = { exists = H18 }
				}
				AND = {
					1974 = { owned_by = ROOT }
					NOT = { exists = H17 }
				}
				AND = {
					1775 = { owned_by = ROOT }
					NOT = { exists = H16 }
				}
			}
		}
		effect = {
			fardach_area = {
				add_permanent_claim = ROOT
			}
			dartir_area = {
				add_permanent_claim = ROOT
			}
			gemradcurt_area = {
				add_permanent_claim = ROOT
			}
			medhan_area = {
				add_permanent_claim = ROOT
			}
			add_mil_power = 25
		}
	}
	tuathak_lone_north = {
		icon = mission_scandinavian_port_authority
		required_missions = { tuathak_tame_the_savage_north } 
		generic = no
		position = 4
		
		trigger = {
			randrunnse_region = {
				is_city = yes
				owned_by = ROOT
			}
		}
		effect = {
			add_prestige = 25
			add_country_modifier = { name = "tuathak_crusade_victorious" duration = 9125 }
		}
	}
	tuathak_end_winters_tyranny = {
		icon = mission_scandinavian_port_authority
		required_missions = { 
			tuathak_lone_north 
		} 
		generic = no
		position = 5
		
		trigger = {
			NOT = { has_country_flag = tuathak_redeem }
			custom_trigger_tooltip = {
				tooltip = EORDAND_Snecboth_Diaspora
				NOT = {
					eordand_superregion = {
						culture = snecboth
						is_city = yes
						NOT = { owned_by = ROOT }
					}
				}
			}
			NOT = {
				average_autonomy = 25
			}
			1775 = {
				OR = {
					has_building = fort_15th
					has_building = fort_16th
					has_building = fort_17th
					has_building = fort_18th
					has_building = fort_magic
				}
			}
			adm_power = 150
			dip_power = 150
			mil_power = 150
		}
		effect = {
			add_adm_power = -150
			add_dip_power = -150
			add_mil_power = -150
			every_owned_province = {
				random = {
					limit = {
						culture = snecboth
					}
					chance = 90
					
					change_culture = tuathak
					change_religion = ROOT
				}
			}
			add_country_modifier = { name = "tuathak_purger_of_winter" duration = 21900 }
			hidden_effect = { set_country_flag = tuathak_purge }
		}
	}
}
tuathak5_missions = {
	slot = 5
	generic = no
	ai = yes 
	potential = {
		primary_culture = tuathak
	}
	
	tuathak_redeem_the_snecboth = {
		icon = mission_scandinavian_port_authority
		required_missions = { 
			tuathak_lone_north
		} 
		generic = no
		position = 5
		
		trigger = {
			NOT = { has_country_flag = tuathak_purge }
			custom_trigger_tooltip = {
				tooltip = EORDAND_Snecboth_Diaspora
				NOT = {
					eordand_superregion = {
						culture = snecboth
						is_city = yes
						NOT = { owned_by = ROOT }
					}
				}
			}
			accepted_culture = snecboth
			development_in_provinces = {
					value = 100
					culture = snecboth
			}
		}
		effect = {
			add_country_modifier = { name = "tuathak_redemption_of_snecboth" duration = 21900" }
			every_owned_province = {
				random = {
					limit = {
						culture = snecboth
					}
					chance = 50
					
					add_province_modifier = { name = "tuathak_loyal_snecboth" duration = 9125 }
				}
			}
			hidden_effect = { set_country_flag = tuathak_redeem }
		}
	}
	
	
}