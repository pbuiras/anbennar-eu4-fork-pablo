
jaddari_westward = {
	slot = 1
	generic = no
	ai = yes
	potential = {
		tag = F46
	}
	has_country_shield = yes
	
	jaddari_mounted_legionaries = {
		icon = mission_abu_saids_dream
		required_missions = { }
		position = 1

		trigger = {
			cavalry_fraction = 0.7
			army_size = 20
		}
		
		effect = {
			add_country_modifier = { 
				name = jaddari_mounted_legions
				duration = 14600 #40 years
			}
		}
	}
	
	jaddari_deal_with_the_xhazobkult = {
		icon = mission_traverse_the_sahara
		required_missions = { jaddari_summon_the_legionaries }
		position = 2

		provinces_to_highlight = {
			OR = {
				area = garpix_zokka_area
			}
			NOT = {
				religion = the_jadd
			}
		}
		trigger = {
			garpix_zokka_area = {
				type = all
				religion = the_jadd
			}
		}
		effect = {
			hidden_effect = {
				bulwar_superregion = {
					limit = {
						culture_group = gnollish
						NOT = { religion = the_jadd }
					}
					add_permanent_claim = ROOT
				}
			}
			custom_tooltip = jaddari_deal_with_the_xhazobkult_tooltip
		}
	}
	
	jaddari_enlighten_the_gnoll = {
		icon = mission_trade_in_gold_coast
		required_missions = { jaddari_deal_with_the_xhazobkult }
		position = 3

		provinces_to_highlight = {
			AND = {
				superregion = bulwar_superregion
				culture_group = gnollish
				NOT = { religion = the_jadd }
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			NOT = {
				bulwar_superregion = {
					culture_group = gnollish
					NOT = { religion = the_jadd }
				}
			}
		}
		effect = {
			add_prestige = 20
			
			largest_increase_of_gnollish_tolerance_effect = yes
			
			hidden_effect = {
				change_variable = {
					which = gnollish_race_tolerance_ai
					value = 30
				}
			}
		}
	}
	
	jaddari_the_western_harpies = {
		icon = mission_prosperity_in_bengal
		required_missions = { jaddari_upper_suran_plain }
		position = 4
		
		provinces_to_highlight = {
			OR = {
				area = crelyore_ridge_area
				area = kalisad_area
				area = arkasul_area
				area = harpylen_area
			}
			NOT = { province_id = 4117 }
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			crelyore_ridge_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			kalisad_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			harpylen_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			669 = { country_or_non_sovereign_subject_holds = ROOT }
			671 = { country_or_non_sovereign_subject_holds = ROOT }
			668 = { country_or_non_sovereign_subject_holds = ROOT }
		}
		effect = {
			add_casus_belli = {
				target = F21
				type = cb_vassalize_mission
				months = 300
			}
			harpy_hills_region = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	jaddari_birsantanses_kneels = {
		icon = mission_rajput_mansabdars
		required_missions = {
			jaddari_the_western_harpies
			jaddari_the_heart_of_bulwar
		}
		position = 5
		
		provinces_to_highlight = {
			OR = {
				province_id = 549
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			OR = {
				F21 = { is_subject_of = ROOT }
				
				AND = {
					NOT = { exists = F21 }
					549 = { country_or_non_sovereign_subject_holds = ROOT }
				}
			}
		}
		effect = {
			bahar_region = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	jaddari_into_bahar = {
		icon = mission_zanzibari_trade
		required_missions = {
			jaddari_birsantanses_kneels
			jaddari_the_suran_delta
		}
		position = 6
		
		provinces_to_highlight = {
			OR = {
				province_id = 526
				province_id = 538
				province_id = 521
				province_id = 516
				province_id = 536
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			526 = { country_or_non_sovereign_subject_holds = ROOT }
			538 = { country_or_non_sovereign_subject_holds = ROOT }
			521 = { country_or_non_sovereign_subject_holds = ROOT }
			516 = { country_or_non_sovereign_subject_holds = ROOT }
			536 = { country_or_non_sovereign_subject_holds = ROOT }
		}
		effect = {
			365 = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			39 = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	jaddari_the_ports_of_businor = {
		icon = mission_malayan_viceroyalty
		required_missions = { jaddari_into_bahar }
		position = 7
		
		provinces_to_highlight = {
			OR = {
				province_id = 365
				province_id = 39
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		trigger = {
			365 = { country_or_non_sovereign_subject_holds = ROOT }
			39 = { country_or_non_sovereign_subject_holds = ROOT }
		}
		effect = {
			add_country_modifier = {
				name = jaddari_shipbuilding_spree
				duration = 3650 #10 years
			}
		}
	}
}

jaddari_bulwar = {
	slot = 2
	generic = no
	ai = yes
	potential = {
		tag = F46
	}
	has_country_shield = yes
	
	jaddari_summon_the_legionaries = {
		icon = mission_conquer_maharashtra
		required_missions = { }
		position = 1
		
		trigger = {
			army_size_percentage = 1
		}
		
		effect = {
			garpix_zokka_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			areab38_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			jikarzax_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			grixek_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			azka_sur_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	jaddari_seize_azka_sur = {
		icon = mission_assert_control_over_delhi
		required_missions = { jaddari_summon_the_legionaries}
		position = 2
		
		provinces_to_highlight = {
			province_id = 643
		}
		
		trigger = {
			643 = { owned_by = ROOT }
		}
			
		effect = {
			far_bulwar_region = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	jaddari_upper_suran_plain = {
		icon = mission_invade_the_deccan_dlh
		required_missions = { jaddari_seize_azka_sur }
		position = 3
		
		provinces_to_highlight = {
			OR = {
				region = far_bulwar_region
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			num_of_owned_provinces_with = {
				value = 20
				region = far_bulwar_region
				OR = {
					owned_by = ROOT
					owner = { is_subject_of = ROOT }
				}
			}
			owns_core_province = 625
		}

		effect = {
			bulwar_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			south_naza_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			east_naza_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			north_naza_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			west_naza_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			harklum_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			kalisad_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			arkasul_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			crelyore_ridge_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			varamhar_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			add_country_modifier = { 
				name = jaddari_rise_of_an_empire
				duration = 14600 #40 years
			}
		}
	}
	
	jaddari_the_heart_of_bulwar = {
		icon = mission_found_the_city_of_agra
		required_missions = { jaddari_upper_suran_plain }
		position = 4
				
		provinces_to_highlight = {
			OR = {
				area = bulwar_area
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			bulwar_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			owns_core_province = 601
		}

		effect = {
			lower_brasan_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			upper_brasan_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			outer_brasan_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			irrliam_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			middle_suran_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			anzarzax_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			anzabad_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			kumarkand_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			596 = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	jaddari_the_suran_delta = {
		icon = mission_convert_bengal
		required_missions = { jaddari_the_heart_of_bulwar }
		position = 5
		
		provinces_to_highlight = {
			OR = {
				area = lower_brasan_area
				area = upper_brasan_area
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			lower_brasan_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			upper_brasan_area = {
				type = all
				country_or_non_sovereign_subject_holds = ROOT
			}
			owns_core_province = 565
		}

		effect = {
			bulwar_proper_region = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	jaddari_the_salahad_gate = {
		icon = mission_imperial_cities
		required_missions = { jaddari_the_suran_delta }
		position = 6
		
		provinces_to_highlight = {
			OR = {
				area = salahad_gate_area
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			salahad_gate_area = {
				type = all
				owned_by = ROOT
				is_core = ROOT
			}
		}

		effect = {
			north_salahad_region = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
		}
	}
	
	jaddari_light_for_kheterata = {
		icon = mission_the_songhai_campaign
		required_missions = { jaddari_the_salahad_gate }
		position = 7
		
		provinces_to_highlight = {
			OR = {
				area = 475
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		
		trigger = {
			owns_core_province = 475
			475 = { religion = the_jadd }
		}

		effect = {
			add_prestige = 20
		}
	}
	
}


jaddari_the_jadd = {
	slot = 3
	generic = no
	ai = yes
	potential = {
		tag = F46
	}
	has_country_shield = yes
	
	jaddari_a_homeland_for_the_jadd = {
		icon = mission_mosque
		required_missions = { }
		position = 1
		
		trigger = {
			calc_true_if = {
				all_province = {
					religion = the_jadd
				}
				amount = 25
			}
			#far_salahad_region = {
			#	type = all
			#	religion = the_jadd
			#}
		}
			
		effect = {
			2909 = {
				add_permanent_province_modifier = {
					name = jaddari_the_mountain_of_clear_sight
					duration = -1
				}
			}
		}
	}
	
	jaddari_build_a_grand_temple = {
		icon = mission_the_sultanate_restored
		required_missions = {
			jaddari_a_homeland_for_the_jadd
			jaddari_seize_azka_sur
		}
		position = 3
		
		trigger = {
			643 = {
				owned_by = ROOT
				has_building = temple
				religion = the_jadd
			}
			dip_power = 100
			adm_power = 100
			treasury = 100
		}

		effect = {
			643 = {
				add_province_modifier = {
					name = jaddari_grand_temple
					duration = -1
				}
			}
			add_dip_power = -100
			add_adm_power = -100
			add_treasury = -100
		}
	}
	
	jaddari_spread_the_faith = {
		icon = mission_subdue_rajputana
		required_missions = { jaddari_build_a_grand_temple }
		position = 4

		trigger = {
			calc_true_if = {
				all_province = {
					religion = the_jadd
				}
				amount = 60
			}
		}
		
		effect = {
			643 = {
				add_base_tax = 2
				add_base_production = 1
				add_base_manpower = 3
				add_province_modifier = {
					name = jaddari_pilgrimages
					duration = 7900 #20 years
				}
			}
			2909 = {
				add_base_tax = 2
				add_base_production = 1
				add_base_manpower = 3
				add_province_modifier = {
					name = jaddari_pilgrimages
					duration = 7900 #20 years
				}
			}
		}
	}
	
	jaddari_reform_the_cult = {
		icon = mission_pacify_punjab
		required_missions = {
			jaddari_spread_the_faith
			jaddari_the_heart_of_bulwar
		}
		position = 5

		trigger = {
			601 = { owned_by = ROOT }
			dip_power = 200
			has_global_modifier_value = {
				which = global_missionary_strength
				value = 0.08
			}
		}
		
		effect = {
			add_dip_power = -200
			every_country = {
				limit = {
					religion = bulwari_sun_cult
				}
				country_event = { 
					id = jaddari_missions.1
					days = 0
					random = 365
				}
			}
			every_province = {
				limit = { religion = bulwari_sun_cult }
				if = {
					limit = { culture = sun_elf }
					random_list = {
						80 = {}
						20 = { change_religion = the_jadd }
					}					
				}
				else = {
					random_list = {
						40 = {}
						60 = { change_religion = the_jadd }
					}					
				}
			}
		}
	}
	
	jaddari_the_true_faith = {
		icon = mission_hands_praying
		required_missions = { jaddari_reform_the_cult }
		position = 6

		trigger = {
			bulwar_superregion = {
				type = all
				religion = the_jadd
			}
		}
		
		effect = {
			every_country = {
				limit = { religion = the_jadd }
				add_country_modifier = {
					name = jaddari_the_true_faith
					duration = -1
				}
			}
		}
	}
}


jaddari_diplomatic = {
	slot = 4
	generic = no
	ai = yes
	potential = {
		tag = F46
	}
	has_country_shield = yes
	
	jaddari_bring_the_harpies_into_the_fold = {
		icon = mission_rein_in_the_velamas
		required_missions = { }
		position = 1
		
		trigger = {
			OR = {
				any_country = {
					primary_culture = siadunan_harpy
					religion = the_jadd
					has_opinion = {
						who = ROOT
						value = 120
					}
				}
			}
		}
		
		effect = {
			every_country = {
				limit = {
					primary_culture = siadunan_harpy
					religion = the_jadd
					has_opinion = {
						who = ROOT
						value = 120
					}
				}
				add_opinion = {
					who = ROOT
					modifier = jaddari_jasiene
				}
				reverse_add_opinion = {
					who = ROOT
					modifier = jaddari_jasiene
				}
			}
			areab33_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			areab37_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			areab36_area = {
				limit = {
					NOT = { is_core = ROOT }
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = ROOT
			}
			
			largest_increase_of_harpy_tolerance_effect = yes
			
			hidden_effect = {
				change_variable = {
					which = harpy_race_tolerance_ai
					value = 30
				}
				F49 = {
					largest_increase_of_elven_tolerance_effect = yes
					change_variable = {
						which = elven_race_tolerance_ai
						value = 30
					}
				}
			}
		}
	}
	
	jaddari_jasienes_heritage = {
		icon = mission_secure_khandesh
		required_missions = { jaddari_bring_the_harpies_into_the_fold }
		position = 2
		
		provinces_to_highlight = {
			OR = {
				id = 2903
				id = 2904
				id = 2906
				id = 2908
				id = 2910
				id = 2919
				id = 2920
				id = 2921
			}
			NOT = {
				country_or_non_sovereign_subject_holds = ROOT
			}
			NOT = {
				owned_by = F49
			}
		}
		
		trigger = {
			F49 = {
				has_opinion = {
					who = ROOT
					value = 200
				}
			}
			2903 = {
				OR = {
					country_or_non_sovereign_subject_holds = ROOT
					owned_by = F49
				}
			}
			2904 = {
				OR = {
					country_or_non_sovereign_subject_holds = ROOT
					owned_by = F49
				}
			}
			2906 = {
				OR = {
					country_or_non_sovereign_subject_holds = ROOT
					owned_by = F49
				}
			}
			2908 = {
				OR = {
					country_or_non_sovereign_subject_holds = ROOT
					owned_by = F49
				}
			}
			2910 = {
				OR = {
					country_or_non_sovereign_subject_holds = ROOT
					owned_by = F49
				}
			}
			2919 = {
				OR = {
					country_or_non_sovereign_subject_holds = ROOT
					owned_by = F49
				}
			}
			2920 = {
				OR = {
					country_or_non_sovereign_subject_holds = ROOT
					owned_by = F49
				}
			}
			2921 = {
				OR = {
					country_or_non_sovereign_subject_holds = ROOT
					owned_by = F49
				}
			}
		}

		effect = {
			F49 = { ## Custom tooltip to explain the mission
				country_event = { 
					id = jaddari_missions.2
					days = 1
				}
			}
		}
	}
	
	jaddari_harpy_diplomats = {
		icon = mission_ottoman_harem
		required_missions = { jaddari_jasienes_heritage }
		position = 3
		
		trigger = {
			dip_power = 400
			OR = {
				AND = {
					accepted_culture = siadunan_harpy
					any_owned_province = { culture = siadunan_harpy }
				}
				any_country = {
					primary_culture = siadunan_harpy
					has_opinion = {
						who = ROOT
						value = 200
					}
				}
			}
		}

		effect = {
			add_dip_power = -400
			add_country_modifier = {
				name = jaddari_harpy_diplomats
				duration = -1
			}
		}
	}
}


jaddari_eastward = {
	slot = 5
	generic = no
	ai = yes
	potential = {
		tag = F46
	}
	has_country_shield = yes
	
	jaddari_fortify_the_northern_pass = {
		icon = mission_fortify_rajputana
		required_missions = { }
		position = 1
		
		provinces_to_highlight = {
			province_id = 2922
		}
		
		trigger = {
			2922 = {
				fort_level = 2
				owned_by = ROOT
			}
		}

		effect = {
			2922 = {
				add_permanent_province_modifier = {
					name = jaddari_fortified_pass
					duration = -1
				}
			}
		}
	}
	
	jaddari_fund_the_empire = {
		icon = mission_zambezi_gold
		required_missions = { jaddari_bring_the_harpies_into_the_fold }
		position = 2
		
		provinces_to_highlight = {
			province_id = 2914
		}
		
		trigger = {
			2914 = { owned_by = ROOT }
		}

		effect = {
			add_country_modifier = {
				name = jaddari_cash_influx
				duration = 7300 #20 years
			}
		}
	}
	
	jaddari_serpentspine_expidition = {
		icon = mission_jawal_silver_mine
		required_missions = { jaddari_fund_the_empire }
		position = 3
		
		provinces_to_highlight = {
			#future
		}
		
		trigger = {
			#future
		}

		effect = {
			#future
		}
	}
	
	jaddari_establish_the_eastern_front = {
		icon = mission_conquer_kannauj
		required_missions = { }
		position = 5
		
		provinces_to_highlight = {
			province_id = 2913
		}
		
		trigger = {
			owns_core_province = 2913
		}

		effect = {
			#future gain claims
		}
	}
	
	jaddari_invade_haless = {
		icon = mission_bengal_campaign
		required_missions = { jaddari_establish_the_eastern_front }
		position = 6
		
		provinces_to_highlight = {
			#future
		}
		
		trigger = {
			#future
		}

		effect = {
			#future
		}
	}
}