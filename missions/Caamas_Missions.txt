caamas1_missions = {
	slot = 1 
	generic = no
	ai = yes 
	potential = {
		primary_culture = caamas
	}
	
	caamas_sponsor_naval_infrastructure = {
		icon = mission_galleys_in_port
		required_missions = {  } 
		generic = no
		position = 1
		
		trigger = {
			navy_size_percentage = 0.9
			num_of_admirals = 1
		}
		effect = {
			add_country_modifier = { name = "caamas_shipbuilding_boom" duration = 7300 }
		}
	}
	
	h01_prepare_for_southern_eordellonian_expedition = {
		icon = mission_look_west
		required_missions = { 
			caamas_sponsor_naval_infrastructure
			caamas_curry_favor_from_merchant_lords
		}
		generic = no
		position = 3
		trigger = {
			OR = {
				num_of_missionaries = 2
				num_of_colonists = 1
			}
		}
		effect = {
			create_explorer = 50
			add_country_modifier = { name = "arakeprun_expedition_preparation" duration = 3650 }
			country_event = {
				id = eordand.4
				days = 3650
				tooltip = EORDAND_EXPEDITION_PREPARATION
			}
		}
	}
	h01_convert_the_fograc = {
		icon = mission_colonise_brazil
		required_missions = { h01_prepare_for_southern_eordellonian_expedition }
		generic = no
		position = 4
		provinces_to_highlight = {
			OR = {
				province_id = 2056
				province_id = 2055
				province_id = 2054
			}
		}
		trigger = {
			owns_core_province = 2056
			2056 = { has_owner_religion = yes }
			owns_core_province = 2055
			2055 = { has_owner_religion = yes }
			owns_core_province = 2054
			2054 = { has_owner_religion = yes }
		}
				
		effect = {
			add_country_modifier = { name = "arakeprun_integration_of_the_fograc" duration = 5475 }
			2056 = {
				add_base_production = 1
			}
			2055 = {
				add_base_production = 1
			}
		}
	}
	
	h01_whispers_among_the_sands = {
		icon = mission_conquer_air
		required_missions = { h01_convert_the_fograc }
		generic = no
		position = 5
		provinces_to_highlight = {
			province_id = 2060
		}
		trigger = {
			owns_core_province = 2060
			2060 = {
				development = 8
			}
			years_of_income = 1.25
		}
		effect = {
			country_event = {
				id = eordand.5
				days = 365
			}
			add_years_of_income = -1.25
		}
	}
	
	h01_unearthed_knowledge_of_cadcimn = {
		icon = mission_consulate_of_the_sea
		required_missions = { h01_whispers_among_the_sands }
		generic = no
		position = 6
		provinces_to_highlight = {
			province_id = 2060
		}
		trigger = {
			2060 = {
				development = 20
				has_building = workshop
			}
			adm_tech = 10
			years_of_income = 1.75
		}
		effect = {
			country_event = {
				id = eordand.6
				days = 360
			}
			add_years_of_income = -1.75
			2060 = {
				add_base_tax = 1
				add_base_production = 2
				add_base_manpower = 1
			}
		}
	}
	
}

caamas2_missions = {
	slot = 2 
	generic = no
	ai = yes 
	potential = {
		primary_culture = caamas
	}
	
	caamas_curry_favor_from_merchant_lords = {
		icon = mission_indian_stateman
		required_missions = { 
			caamas_sponsor_naval_infrastructure
			caamas_bolster_internal_trade
		} 
		generic = no
		position = 2
		
		trigger = {
			prestige = 30
			navy_size = 20
		}
		effect = {
			add_navy_tradition = 20
			add_sailors = 2000
		}
	}
	caamas_mare_sarmadfar = {
		icon = mission_rb_colonise_spice_islands
		required_missions = { 
			caamas_curry_favor_from_merchant_lords
			caamas_merchant_holds
		} 
		generic = no
		position = 4
		
		trigger = {
			home_trade_node = {
				is_strongest_trade_power = ROOT
			}
		}
		effect = {
			add_country_modifier = { name = "caamas_sarmadfar_trade_dominance" duration = 18250 }
		}
	}
	caamas_venture_further_south = {
		icon = mission_rb_grow_the_channel
		required_missions = { 
			caamas_mare_sarmadfar
			h01_convert_the_fograc
		} 
		generic = no
		position = 5

		trigger = {
			colonial_haraf = {
					has_discovered = ROOT
			}
		}
		effect = {
			add_country_modifier = { name = "caamas_haraf_interests" duration = 7300 }
		}
	}
	caamas_foothold_in_haraf = {
		icon = mission_caribbean_cn
		required_missions = { 
			caamas_venture_further_south
		} 
		generic = no
		position = 6
		
		trigger = {
			num_of_provinces_owned_or_owned_by_non_sovereign_subjects_with = {
					value = 3
					colonial_region = colonial_haraf
					is_city = yes
			}
		}
		effect = {
			add_country_modifier = { name = "caamas_integrating_haraf" duration = 7300 }
		}
	}
	caamas_urdea_samrad = {
		icon = mission_conquer_alexandria
		required_missions = { 
			caamas_foothold_in_haraf
		} 
		generic = no
		position = 7
		
		trigger = {
			owns_core_province = 2088
		}
		effect = {
			2088 = {
				change_culture = caamas
				change_province_name = "Urdea-Samrad" #New South Summer
				rename_capital = "Urdea-Samrad"
				add_province_modifier = { name = "caamas_urdea_samrad" duration = -1 }
			}
		}
	}
	
}
caamas3_missions = {
	slot = 3 
	generic = no
	ai = yes 
	potential = {
		primary_culture = caamas
	}
	
	caamas_bolster_internal_trade = {
		icon = mission_dominate_home_trade_node
		required_missions = {  } 
		generic = no
		position = 1
		
		trigger = {
			capital_scope = {
				OR = {
					any_province_in_state = {
						province_has_center_of_trade_of_level = 1
						base_production = 6
					}
					any_province_in_state = {
						NOT = { province_has_center_of_trade_of_level = 1 }
						base_production = 6
						base_manpower = 4
					}
				}
			}
		}
		effect = {
			capital_scope = {
				limit = {
					base_production = 6
				}
				add_province_modifier = { name = "caamas_trade_secured" duration = 7300 }
			}
		}
	}
	caamas_merchant_holds = {
		icon = mission_dominate_genoa
		required_missions = { 
			caamas_bolster_internal_trade
			caamas_curry_favor_from_merchant_lords
		} 
		generic = no
		position = 3
		
		trigger = {
			estate_influence =  {
				estate = estate_burghers
				influence = 60
			}
			estate_loyalty = {
				estate = estate_burghers
				loyalty = 60
			}
		}
		effect = {
			add_country_modifier = { name = "caamas_merchant_hold" duration = 9125 } 
		}
	}
	caamas_floating_fortresses = {
		icon = mission_rb_a_mighty_fleet
		required_missions = { 
			caamas_merchant_holds
			caamas_mare_sarmadfar
		} 
		generic = no
		position = 5
		
		trigger = {
			num_of_heavy_ship = 10
		}
		effect = {
			add_country_modifier = { name = "caamas_floating_fortresses" duration = 9125 }
		}
	}

	
}
caamas4_missions = {
	slot = 4
	generic = no
	ai = yes 
	potential = {
		primary_culture = caamas
	}
	
	caamas_fortify_bulwark_ruins = {
		icon = mission_break_sicily
		required_missions = { caamas_bolster_internal_trade } 
		generic = no
		position = 2
		
		trigger = {
			calc_true_if = {
				all_owned_province = {
					OR = {
						has_building = fort_15th
						has_building = fort_16th
						has_building = fort_17th
						has_building = fort_18th
						has_building = fort_magic
					}
				}
				amount = 2
			}
		}
		effect = {
			every_core_province = {
				limit = { 
					OR = {
						has_building = fort_15th
						has_building = fort_16th
						has_building = fort_17th
						has_building = fort_18th
						has_building = fort_magic
					} 
				}
				add_base_production = 1
				add_base_manpower = 1
			}
		}
	}
	caamas_unite_summer_court = {
		icon = mission_plus_ultra
		required_missions = { caamas_fortify_bulwark_ruins } 
		generic = no
		position = 3
		
		trigger = {
			owns_core_province = 2022
			owns_core_province = 2028
			owns_core_province = 2041
			owns_core_province = 2037
		}
		effect = {
			add_country_modifier = { name = "caamas_summer_union" duration = 18250 }
			eordand_superregion = {
				limit = {
					culture = caamas
					NOT = { owned_by = ROOT }
				}
				add_core = ROOT
			}
		}
	}
	
}
caamas5_missions = { #Murdkather
	slot = 5
	generic = no
	ai = yes 
	potential = {
		tag = H02
	}
	
	murdkather_elarbarc_tensions = {
		icon = mission_portuguese_malacca
		required_missions = {  } 
		generic = no
		position = 1
		
		trigger = {
			2053 = {
				owned_by = ROOT
			}
			2052 = {
				owned_by = ROOT
			}
		}
		effect = {
			2053 = {
				add_province_modifier = { name = "billel_elarbarc_toll" duration = -1 }
			}
		}
	}
	murdkather_autumn_passage = {
		icon = mission_conquer_50_development
		required_missions = { murdkather_elarbarc_tensions } 
		generic = no
		position = 2
		
		trigger = {
			2045 = {
				country_or_non_sovereign_subject_holds = ROOT
			}
			trimgarb_area = {
				country_or_non_sovereign_subject_holds = ROOT
				type = all
			}
		}
		effect = {
			2045 = {
				add_province_modifier = { name = "murdkather_rebellious_peitar" duration = 9125 }
			}
			trimgarb_area = {
				add_province_modifier = { name = "murdkather_rebellious_peitar" duration = 9125 }
			}
		}
	}
	murdkather_summer_shipyards = {
		icon = mission_aragonese_navy
		required_missions = { murdkather_autumn_passage } 
		generic = no
		position = 3
		
		trigger = {
			2037 = {
				has_building = shipyard
			}
			2043 = {
				base_production = 5
				has_building = workshop
			}
			2051 = {
				base_production = 5
				has_building = workshop
			}
		}
		effect = {
			2037 = {	
				add_province_modifier = { name = "arakeprun_elarbarc_shipyards" duration = -1}
			}
		}
	}
	murdkather_randrunnse_ports = {
		icon = mission_danzig_or_war
		required_missions = { murdkather_summer_shipyards } 
		generic = no
		position = 4
		
		trigger = {
			1965 = {
				country_or_non_sovereign_subject_holds = ROOT
			}
			1809 = {
				country_or_non_sovereign_subject_holds = ROOT
			}
		}
		effect = {
			add_country_modifier = { name = "murdkather_randrunnse_ports" duration = 10950 }
		}
	}
	murdkather_snecboth_fur_trade = {
		icon = mission_rb_colonise_canada
		required_missions = { murdkather_randrunnse_ports } 
		generic = no
		position = 5
		
		trigger = {
			fur = 5
			all_owned_province = {
				limit = {
					trade_goods = fur
				}
				base_production = 3
			}
		}
		effect = {
			random_owned_province = {
					limit = {
						trade_goods = fur
					}
					add_base_manpower = 1
					add_base_production = 1
					add_permanent_province_modifier = {
						name = murdkather_fur_trade
						duration = 18250
					}
			}
			random_owned_province = {
					limit = {
						trade_goods = fur
					}
					add_base_manpower = 1
					add_base_production = 1
					add_permanent_province_modifier = {
						name = murdkather_fur_trade
						duration = 18250
					}
			}
			random_owned_province = {
					limit = {
						trade_goods = fur
					}
					add_base_manpower = 1
					add_base_production = 1
					add_permanent_province_modifier = {
						name = murdkather_fur_trade
						duration = 18250
					}
			}
		}
	}
}
caamas5_missions = { #Drisleak
	slot = 5
	generic = no
	ai = yes 
	potential = {
		tag = H03
	}
	
	drisleak_hold_billel = {
		icon = mission_combat_barbary_piracy
		required_missions = { } 
		generic = no
		position = 1
		
		trigger = {
			2053 = {
				owned_by = ROOT
				OR = {
					has_building = fort_15th
					has_building = fort_16th
					has_building = fort_17th
					has_building = fort_18th
					has_building = fort_magic
				}
			}
		}
		effect = {
			2053 = {
				add_province_modifier = { name = "drisleak_billel_fortifications" duration = -1 }
				add_base_manpower = 1
			}
		}
	}
	drisleak_eclipse_murdkather = {
		icon = mission_conquer_lower_burma_ori
		required_missions = { drisleak_hold_billel } 
		generic = no
		position = 2
		
		trigger = {
			2028 = {
				owned_by = ROOT
				development = 15
				has_building = temple
			}
		}
		effect = {
			prestige = 10
			2028 = {
				add_base_tax = 1
				add_base_production = 1
				add_base_manpower = 1
				center_of_trade = 1
			}
		}
	}
	drisleak_curb_the_bagcatiran_lords = {
		icon = mission_disrupt_portuguese_trade
		required_missions = { drisleak_eclipse_murdkather } 
		generic = no
		position = 3
		
		trigger = {
			2022 = {
				owned_by = ROOT
				NOT = { local_autonomy = 25 }
			}
		}
		effect = {
			2022 = {
				add_province_modifier = { name = "drisleak_western_port" duration = 9125 }
			}
		}
	}	
	
}
caamas5_missions = { #Bagcatir
	slot = 5
	generic = no
	ai = yes 
	potential = {
		tag = H07
	}
	
	bagcatir_city_of_free_merchants = {
		icon = mission_control_venice
		required_missions = { } 
		generic = no
		position = 1
		
		trigger = {
			republican_tradition = 90
			OR = {
				adm = 5
				dip = 5
				mil = 5
			}
		}
		effect = {
			add_treasury = 50
			add_stability = 1
		}
	}
	bagcatir_capitalize_on_elarbarc_tensions = {
		icon = mission_combat_barbary_piracy
		required_missions = { bagcatir_city_of_free_merchants } 
		generic = no
		position = 2
		
		trigger = {
			2053 = {
				owned_by = ROOT
			}
			OR = {
				2028 = {
					owned_by = ROOT
				}
				2037 = {
					owned_by = ROOT
				}
			}
		}
		effect = {
			paskala_area = {
				add_permanent_claim = ROOT
			}
			dearktir_area = {
				add_permanent_claim = ROOT
			}
			murdkather_area = {
				add_permanent_claim = ROOT
			}
			add_country_modifier = { name = "bagcatir_elarbarc_supremacy" duration = 10950 }
		}
	}
	bagcatir_control_the_south_sarmadfar = {
		icon = mission_rb_take_gibraltar
		required_missions = { bagcatir_capitalize_on_elarbarc_tensions } 
		generic = no
		position = 3
		
		trigger = {
			2056 = {
				owned_by = ROOT
			}
			2055 = {
				owned_by = ROOT
			}
			2054 = {
				owned_by = ROOT
			}
			2061 = {
				owned_by = ROOT
			}
		}
		effect = {
			2022 = {
				add_base_tax = 1
				add_base_production = 1
				add_base_manpower = 1
			}
			add_country_modifier = { name = "bagcatir_colonial_spirit" duration = 9125 }
		}
	}
}