# A province can only belong to one region!

# random_new_world_region is used for RNW. Must be first in this list.
random_new_world_region = {
}

########################################
# AELANTIR		                       #
########################################

# North Aelantir 

tor_nayyi_region = {
	areas = {
		areatd16_area
		areatd17_area
		areatd18_area
		areatd19_area
		areatd20_area
		
		areate14_area
		areate15_area
	}
}

epednan_expanse_region = {
	areas = {
		areate29_area
		areate30_area
		areate31_area
		areate32_area
		areate35_area
		areate36_area
		
		areate46_area
		areate60_area
		areate61_area
		areate66_area
	}
}

haraf_region = {
	areas = {
		areate1_area
		areate2_area
		areate3_area
		areate4_area
		areate5_area
		areate6_area
		areate7_area
		areate8_area
		areate9_area
		areate10_area
		areate11_area
		areate12_area
		areate13_area
		areate16_area
		areate17_area
		areate18_area
		zurzumexia_area
		areate20_area
		areate21_area
		mestiikardu_area
		areate23_area
		areate24_area
		areate25_area
		gommiochand_area
		areate27_area
		areate28_area
	}
}

mushroom_forest_region = {
	areas = {
		areatb79_area
		areatb87_area
		areatb88_area
	}
}

amadia_region = {
	areas = {
		areatb56_area
		areatb57_area
		areatb58_area
		areatb59_area
		areatb60_area
		areatb61_area
		areatb62_area
		areatb63_area
		areatb64_area
		areatb65_area
		areatb66_area
		areatb67_area
		areatb68_area
		areatb69_area
		areatb70_area
		areatb71_area
		areatb72_area
		areatb73_area
		areatb74_area
		areatb75_area
		areatb76_area
		areatb78_area
	}
}

larankarha_highlands_region = {
	areas = {
		areatb89_area
		areatb90_area
		areatb91_area
		areatb96_area
		areatb97_area
		areatb98_area
		areatb99_area
		areatc3_area
	}
}

taychend_region = {
	areas = {
		areatc1_area
		areatc2_area
		areatc4_area
		areatc5_area
		areatc6_area
		areatc7_area
		areatc8_area
		areatc9_area
		areatc10_area
		areatc11_area
		areatc12_area
		areatc13_area
		areatc14_area
		areatc15_area
		areatc16_area
		areatc17_area
		areatc18_area
		areatc19_area
		areatc20_area
		areatc21_area
		areatc22_area
		areatc23_area
		areatc29_area
		areatc30_area
		areatd29_area
	}
}

chendhya_region = {	#the middle lands
	areas = {
		areatc24_area
		areatc25_area
		areatc26_area
		areatc27_area
		areatc28_area
		areatc31_area
		areatc32_area
		areatc33_area
		areatb92_area
		areatb93_area
		areatb94_area
		areatb95_area
		areatc58_area
		areatc63_area
		areatc64_area
	}
}

severed_coast_region = {
	areas = {
		areatc89_area
		areatd30_area
		areatd31_area
		areatd32_area
		areatd33_area
		areatd34_area
	}
}

mteibas_valley_region = {
	areas = {
		areatc39_area
		areatc40_area
		areatc42_area
		areatc47_area
		areatc48_area
		areatc50_area
		areatc56_area
		areatc57_area
		areatc61_area
		areatc62_area
	}
}

west_effelai_region = {
	areas = {
		silver_banks_area
		hungry_jungle_area
		the_twins_area
		silent_expanse_area
	}
}

andic_reach_region = {
	areas = {
		areatc34_area
		areatc35_area
		areatc36_area
		areatc37_area
		areatc38_area
		areatc41_area
		areatc43_area
		areatc44_area
		areatc45_area
		areatc46_area
		
	}
}

devand_region = {	#the boys cleaved each other
	areas = {
		areatc49_area
		areatc51_area
		areatc52_area
		areatc53_area
		areatc54_area
		areatc55_area
		areatc59_area
		areatc60_area
		areatc65_area
		areatc66_area
		areatc67_area
		# areatc69_area # Also in Dry Coast
		
		areatc91_area
		areatc92_area
	}
}

alecand_region = {
	areas = {
		areatd1_area
		areatd2_area
		areatd3_area
		areatd4_area
		areatd5_area
		areatd6_area
		areatd7a_area
		areatd7b_area
		areatd8_area
		areatd9_area
		areatd10_area
		areatd11_area
		areatd12_area
		areatd13_area
		areatd14_area
		areatd15_area
		
		areatc93_area
	}
}


east_effelai_region = {
	areas = {
		areatc74_area
		areatc75_area
		areatc76_area
		areatc77_area
		areatc78_area
		areatc79_area
		areatc80_area
		areatc81_area
		areatc82_area
		areatc83_area
		areatc90_area
		areatd21_area
		areatd22_area
		areatd23_area
		areatd28_area
	}
}

south_effelai_region = {
	areas = {
		mistwood_area
		deep_trail_area
		central_effelai_area
		rockfall_jungle_area
		junglemouth_area
	}
	monsoon = {
		00.01.01
		00.05.30
	}
}

dry_coast_region = {
	areas = {
		areatc69_area # Also in devand
		areatc70_area
		areatc71_area
		areatc72_area
		areatc73_area
		areatd24_area
		areatd25_area
		areatd26_area
		areatd27_area
	}
}

leechdens_region = {
	areas = {
		serpenthome_area
		spider_coast_area
		screaming_jungle_area
		middans_area
		edgewoods_area
		ungoths_trail_area
		soaring_forest_area
		red_isle_area
		croakwood_area
		leechdens_maw_area
	}
	monsoon = {
		00.12.01
		00.12.30
	}
	monsoon = {
		00.01.01
		00.04.30
	}
}

turtleback_isle_region = {
	areas = {
		areatd35_area
		areatd36_area
		areatd37_area
		areatd38_area
	}
}

north_aelantir_region = {
	areas = {
		areaae1_area
		areaae2_area
	}
}

endralliande_region = {
	areas = {
		areaae3_area
		areaae4_area
		areaae5_area
		areaae6_area
		areaae7_area
		areaae8_area
		areaae9_area
		areaae10_area
		areaae11_area
		munasport_area
		inner_endralliande_area
	}
}

ravenous_isle_region = {
	areas = {
		areaae12a_area
		areaae12b_area
		areaae13_area
		areaae14_area
		areaae15_area
	}
}

ruined_isles_region = {
	areas = {
		arears1_area
		arears2_area
		arears3_area
		arears4_area
		arears5_area
		arears7_area
		arears8_area
		arears9_area	#these four should be changed when we do the south bit of that place
		arears10_area
		arears11_area
		arears12_area
		wanderers_gate_area
		calamity_islands_area
		areaae16_area
		
		saamirses_area
	}
}

soruin_region = {
	areas = {
		arears6_area
		areatb40_area
		areatb41_area
		areatb42_area
		areatb43_area
		greenfort_area
		areatb45_area
		areatb46_area
		areatb47_area
		areatb48_area
		areatb49_area
	}
}

trollsbay_region = {
	areas = {
		valorpoint_area
		areatb2_area
		coast_of_isobel_area
		areatb4_area
		areatb5_area
		zanlib_area
		ynnsmouth_area
		marlliande_area
		areatb9_area
		cestirmark_area
		areatb11_area
		areatb12_area
		areatb15_area
		areatb16_area
		areatb17_area
		areatb18_area
		areatb19_area
		areatb20_area
		areatb26_area
		areatb27_area
		areatb28_area
		areatb29_area
		areatb31_area
		thilsvis_area
	}
}

reapers_coast_region = {
	areas = {
		areatb30_area
		areatb32_area
		areatb33_area
		areatb34_area
		areatb35_area
		areatb36_area
		areatb37_area
	}
}

dalaire_region = {
	areas = {
		stalwart_outpost_area
		teirawood_border_area
		areatda1_area
		areatda2_area
		areatda3_area
		areatda4_area
		areatda5_area
		areatda6_area
		areatda7_area
		areatda8_area
		areatda9_area
		areatda10_area
		areatda11_area
		areate94_area
		areate92_area
		areate98_area
		areate99_area
	}
}

sarda_region = {
	areas = {
		areate33_area
		areate34_area
		areate37_area
		areate38_area
		areate39_area
		areate40_area
		areate41_area
		areate42_area
		areate43_area
		areate47_area
		areate48_area
		areate49_area
		areate103_area
	}
}

dolindha_region = {
	areas = {
		areate52_area
		ebenmas_area
		areate54_area
		areate56_area
		areate57_area
		
		areate62_area
		areate64_area
		areate80_area
		areate81_area
		areate85_area
		
		areate104_area
		
		grebniesth_area
	}
}

rzenta_region = {
	areas = {
		areate65_area
		areate68_area
		areate69_area
		areate70_area
		areate71_area
		areate72_area
		areate75_area
		areate83_area
		areate74_area
		areate76_area
		areate79_area
	}
}

veykoda_region = {
	areas = {
		areate44_area
		areate45_area
		corinsfield_area
		balgabar_area
		areate55_area
		
		areate100_area
		areate101_area
		areate102_area

	}
}

forest_of_the_cursed_ones_region = {
	areas = {
		areate58_area
		areate59_area
		areate63_area
		areate82_area
		areate86_area
		areate88_area
		areate89_area
		areate93_area
		areate84a_area
	}
}

bloodgroves_region = {
	areas = {
		south_dalairey_coast_area
		areatb13_area
		areatb14_area
		bloodbrook_area
		areatb21_area
		areatb22_area
		areatb23_area
		areatb24_area
		areatb25_area
		areatb38_area
	}
}

randrunnse_region = {
	areas = {
		tamharcroc_area
		fogharbac_area
		fardach_area
		dartir_area
		medhan_area
		trimgarb_area
		galbhan_area
		anholtir_area
		armonadh_area
		jhorgashirr_area
		gemradcurt_area
	}
}

glorelthir_region = {
	areas = {
		darhan_area
		monkast_area
		trasand_area
		drisleak_area
		iadth_area
		ruigfar_area
		sglolad_area
		raithlos_area
	}
}

sarmadfar_region = {
	areas = {
		serakriok_area
		areaeor5_area
		areaeor6_area
		areaeor17_area
		murdkather_area
		sidmargbal_area
		dearktir_area
		paskala_area
		gathgob_area
		pelodard_area
		eorwestislands_area
	}
}

broken_isles_region = {
	areas = {
		areaeor1_area
		areaeor2_area
		areaeor3_area
		areate78_area
		themaria_area
		areate90_area
		areate91_area
		areate95_area
		areate96_area
	}
}
########################################
# GERUDIA		                       #
########################################

gerudian_coast_region = {
	areas = {
		southern_giants_tears_area
		northern_giants_tears_area
		esfjall_area
		naugsvol_area
		nyrford_area
		olavslund_area
		elkaesals_slumber_area
		giants_manse_area
		gullmork_area
		gifrbygd_area
	}
}

dalr_region = {
	areas = {
		frostbridges_area
		bjarnland_area
		alptborg_area
		revrland_area
		fegras_area
		ismark_area
		sidaett_area
		dalrfjall_area
		thurrsbol_area
		leifsborg_area
		kaldrland_area
	}
}

########################################
# WESTERN CANNOR                       #
########################################

isles_of_lament_region = {
	areas = {
		far_isle_area
	}
}

lencenor_region = {
	areas = {
		venail_area
		upper_bloodwine_area
		lower_bloodwine_area
		sorncost_vines_area
		sornhills_area
		shrouded_coast_area
		ionnidar_area
		redglades_area
		ruby_mountains_area
		deranne_area
		northern_flats_area
		southern_flats_area
		darom_area
		eastern_winebay_area
		upper_winebay_area
		lower_winebay_area
		southroy_area
	}
}

small_country_region = {
	areas = {
		viswall_area
		roysfort_area
		the_borders_area
		thomsbridge_area
		applefields_area
		lorenans_reach_area
		lorentish_approach_area
		pearpoint_area
		norley_area
	}
}

dragon_coast_region = {
	areas = {
		gnomish_pass_area
		nimscodd_area
		dragonpoint_area
		dragondowns_area
		dragonspine_area
		dragonheights_area
		dragondepth_area
		storm_isles_area
		iochand_area
		reaver_coast_area
		dragonhills_area
		lonely_isle_area
	}
}

west_dameshead_region = {
	areas = {
		wesdam_area
		neckcliffe_area
		damespearl_area
		pearlywine_area
		carneteria_area
		windtower_area
		tretunica_area
		exwes_area
		woodwell_area
	}
}

east_dameshead_region = {
	areas = {
		west_damesear_area
		east_damesear_area
		eastneck_area
		damerian_dales_area
		silverwoods_area
		menibor_loop_area
		middle_luna_area
		the_commons_area
		upper_luna_area
		verne_area
		wyvernmark_area
		galeinn_area
		heartlands_area
	}
}

the_borders_region = {
	areas = {
		wexhills_area
		east_bisan_area
		west_bisan_area
		ottocam_area
		greater_bardswood_area
		eastborders_area
		gnollsgate_area
		gisden_area
		arannen_area
		rhinmond_area
		hawkfields_area
		antir_drop_area
		highcliff_area
	}
}

forlorn_vale_region = {
	areas = {
		north_ibevar_area
		south_ibevar_area
		silent_repose_area
		teagansfield_area
		cursewood_area
		rotwall_area
		whistlevale_area
	}
}

esmaria_region = {
	areas = {
		konwell_area
		ryalanar_area
		high_esmar_area
		low_esmar_area
		hearthswood_area
		bennonhill_area
		cann_esmar_area
		songbarges_area
		ashfields_area
	}
}

businor_region = {
	areas = {
		lorbet_area
		mountainway_area
		busilar_area
		busilari_straits_area
		hapiande_area
		khenak_area
		isle_of_tef_area
	}
}

alenic_frontier_region = {
	areas = {
		westmoor_proper_area
		moorhills_area
		beronmoor_area
		westmounts_area
		alenic_expanse_area
		gawed_area
		south_alen_area
		alenvord_area
		balvord_area
		jonsway_area
		arbaran_area
		golden_plains_area
		cestir_area
		coddorran_heights_area
	}
}

alenic_reach_region = {
	areas = {
		mawriver_area
		serpentshead_area
		northern_greatwoods_area
		dinesck_area
		drowned_giant_isles_area
		west_chillsbay_area
		east_chillsbay_area
		wudhal_area
		sondaar_area
		vrorenwall_area
		cedesck_area
		gulletpeak_area
		vrorenmarch_area
	}
}

damescrown_region = {
	areas = {
		damescrown_area
		floodmarches_area
		vertesk_area
		dames_forehead_area
		beepeck_area
		derwing_area
		heartland_borders_area
		ardail_island_area
	}
}

dostanor_region = {
	areas = {
		corvurian_plains_area
		baldostan_area
		central_corvuria_area
		blackwoods_area
	}
}

daravans_folly_region = {
	areas = {
		dreadmire_area
		flooded_coast_area
	}
}

inner_castanor_region = {
	areas = {
		castonath_area
		upper_nath_area
		lower_nath_area
		westgate_area
		southgate_area
		trialmount_area
		steelhyl_area
		nortmere_area
		area71_area
		serpentsmarck_area
		area73_area
		ardent_glade_area
		area75_area
		area76_area
		area77_area
		area78_area
		area80_area
		coalwoud_area
	}
}

south_castanor_region = {
	areas = {
		burnoll_area
		dostans_way_area
		marrhold_area
		oudmerewood_area
		area54_area
		devaced_area
		vernham_area
		oudeben_area
		beastgrave_area
		clovenwood_area
		banesfork_area
		blademarch_area
		swapstoke_area
		hornwood_area
		doewood_area
		themin_area
		humacvord_area
		dryadsdale_area
		area82_area
		area83_area
	}
}

west_castanor_region = {
	areas = {
		balmire_area
		westwall_approach_area
		middle_alen_area
		athfork_area
		ebonmarck_area
		alenwood_area
		falsemire_area
		silvervord_area
		adenica_area
		upcreek_area
		rohibon_area
		area49_area
		taran_plains_area
		verteben_area
		merescker_area
		nortmerewood_area
	}
}


akan_region = {
	areas = {
		deshak_area
		ekha_area
		middle_akan_area
		east_akan_area
		central_akan_area
		west_akan_area
		akan_isles_area
	}
}

north_salahad_region = {
	areas = {
		area32_area
		area33_area
		area34_area
		coast_of_tears_area
		mothers_delta_area
		area35_area
		lower_sorrow_area
		kheterat_proper_area
		upper_sorrow_area
		area37_area
		area38_area
		elizna_area
		area39_area
	}
}

gol_region = {
	areas = {
		golkora_stretch_area
		gnollakaz_area
		area36_area
	}
}

########################################
# Bulwar				  		#
########################################

ourdia_region = {
	areas = {
		west_overmarch_area
		east_overmarch_area
		south_overmarch_area
	}
}

bahar_region = {
	areas = {
		crathanor_area
		reuyel_area
		medbahar_area
		area_f1_area
		tungr_mountains_area
		aqatbahar_area
		azka_evran_area
		bahar_szel_uak_area
		area_f3_area
	}
}

bulwar_proper_region = {
	areas = {
		inner_drolas_area
		outer_drolas_area
		lower_brasan_area
		upper_brasan_area
		outer_brasan_area
		anzabad_area
		irrliam_area
		salahad_gate_area
		sur_mountains_area
		garpix_ukraz_area
		garpix_tluukt_area
		grixekyr_area
		zansap_area
		middle_suran_area
		anzarzax_area
		kumarkand_area
		bulwar_area
		varamhar_area
		yrkur_area
		harklum_area
		south_naza_area
		west_naza_area
		north_naza_area
		east_naza_area	
	}
}

harpy_hills_region = {
	areas = {
		area_hh0a_area
		area_hh0b_area
		areahh8_area
		alyzksaan_area
		upper_gelkali_area
		gelkalis_area
		jorkad_lake_area
		harpylen_area
		crelyore_ridge_area
		arkasul_area
		kalisad_area
		invaders_pass_area
	}
}

far_bulwar_region = {
	areas = {
		avamezan_area	
		eduz_vacyn_area
		panuses_area	
		upper_suran_area	
		azka_sur_area	
		grixek_area	
		ardu_area	
		garpix_zokka_area	
		jikarzax_area	
		sareyand_area
	}
}

far_salahad_region = {
	areas = {
		areab32_area
		areab33_area
		areab34_area
		areab35_area
		areab35_area
		areab36_area
		areab37_area
		areab38_area
		areab39_area
		areab40_area
	}
}

#Sea Regions
north_aelantir_coast_region = {
	areas = {
		north_aelantir_coast_area
	}
}

south_aelantir_coast_region = {
	areas = {
		south_aelantir_coast_area
		leechdens_area
	}
}

north_uelos_lament_ocean_region = {
	areas = {
		edillions_relief_area
		far_sea_area
		the_great_expanse_area
		banished_sea_area
		coast_of_endrallian_area
	}
}

south_uelos_lament_ocean_region = {
	areas = {
		south_uelos_lament_sea_area
		far_uelos_lament_sea_area
	}
}

south_sarhal_coast_region = {
	areas = {
		splintered_archipelago_area
		kedwali_coast_area
	}
}

west_salahad_coast_region = {
	areas = {
		west_salahad_sea_area
	}
}

cleaved_sea_coast_region = {
	areas = {
		kaydhano_coast_area
		cleaved_sea_area
	}
}

north_insyaa_ocean_region = {
	areas = {
		north_insyaa_sea_area
		bay_of_insyaa_area
		barrier_islands_area
	}
}

south_insyaa_ocean_region = {
	areas = {
		south_insyaa_sea_area
	}
}

punctured_coast_region = {
	areas = {
		punctured_coast_area
	}
}

west_aelantir_coast_region = {
	areas = {
		west_aelantir_coast_area
	}
}

torn_sea_region = {
	areas = {
		torn_sea_area
	}
}

outer_torn_sea_region = {
	areas = {
		outer_torn_sea_area
	}
}


northern_pass_region = {
	areas = {
		northern_pass_area
		inner_sea_area
		outer_sea_area
	}
}

ringlet_sea_region = {
	areas = {
		ringlet_sea_area
		the_bridge_area
	}
}

gulf_of_rahen_region = {
	areas = {
		gulf_of_rahen_area
	}
}

northeast_haless_coast_region = {
	areas = {
		northeast_haless_coast_area
	}
}

kedwali_gulf_region = {
	areas = {
		kedwali_gulf_area 
	}
}

dameshead_sea_region = {
	areas = {
		damesneck_sea_area
		dameshead_sea_area
	}
}

giants_grave_sea_region = {
	areas = {
		giants_grave_sea_area
		bay_of_chills_area
	}
}

divenhal_sea_region = {
	areas = {
		bay_of_wines_sea_area
		west_diven_area
		coast_of_akan_area
		east_diven_area
		sea_of_follies_area
		sea_of_stone_area
		gulf_of_glass_area
		brasanni_sea_area
	}
}

westcoast_region = {
	areas = {
		southcoast_area
		westcoast_area
		bay_of_dragons_area
	}
}

northern_thaw_region = {
	areas = {
		reaversea_area
		far_thaw_area
		lonely_sea_area
	}
}

broken_sea_region = {
	areas = {
		broken_sea_approach_area
		broken_sea_area
	}
}

western_broken_sea_region = {
	areas = {
		western_broken_sea_area
	}
}

ruined_sea_region = {
	areas = {
		trollsbay_coast_area
		ruined_sea_area
		east_ruin_sea_area
		western_respite_sea_area
		reapers_coast_area
	}
}

calamity_pass_region = {
	areas = {
		calamity_pass_area
	}
}


########################################
# SERPENTSPINE                         #
########################################


serpents_vale_region = {
	areas = {
		areadw02_area
		areadw03_area
		areadw04_area
		areadw05_area
	}
}

northern_pass_region = {
	areas = {
		areadw06_area
		areadw07_area
		areadw08_area
		areadw09_area
		areadw10_area
		areadw11_area
		areadw12_area
		areadw13_area
	}
}

west_dwarovar_region = {
	areas = {
		areadw01_area
		areadw14_area
		areadw15_area
		areadw16_area
		areadw17_area
		areadw18_area
		areadw19_area
		areadw20_area
		areadw21_area
		areadw22_area
		areadw23_area
		areadw23_area
		areadw24_area
		areadw25_area
		areadw26_area
		areadw27_area
		areadw28_area
		areadw29_area
		areadw30_area
		areadw31_area
		areadw32_area
		areadw33_area
		areadw34_area
		areadw35_area
		areadw36_area
		areadw37_area
		areadw38_area
		areadw39_area
		areadw40_area
		areadw41_area
		areadw42_area
		areadw43_area
		areadw44_area
		areadw45_area
		areadw46_area
		areadw47_area
		areadw48_area
		areadw49_area
		areadw50_area
	}
}

middle_dwarovar_region = {
	areas = {
		areadw51_area
		areadw52_area
		areadw53_area
		areadw54_area
		areadw55_area
		areadw56_area
		areadw57_area
		areadw58_area
		seghdihr_area
		areadw60_area
		areadw61_area
		areadw62_area
		areadw63_area
		verkal_gulan_area
		areadw65_area
		areadw66_area
		areadw67_area
		areadw68_area
	}
}

serpentreach_region = {
	areas = {
		areadw69_area
		areadw70_area
		areadw71_area
		areadw72_area
		areadw73_area
		areadw74_area
		areadw75_area
		areadw76_area
		areadw77_area
		areadw78_area
		areadw79_area
		areadw80_area
		areadw81_area
		areadw82_area
		areadw83_area
		areadw84_area
		areadw85_area
		areadw86_area
		areadw87_area
	}
}

tree_of_stone_region = {
	areas = {
		areadw88_area
		areadw89_area
		areadw90_area
		areadw91_area
		areadw92_area
		areadw93_area
		areadw94_area
		areadw95_area
		areadw96_area
		areadw97_area
		areadw98_area
		areadw99_area
		areadw100_area
		areadw101_area
		areadw102_area
		areadw103_area
	}
}

jade_mines_region = {
	areas = {
		areadw104_area
		areadw105_area
		areadw106_area
		areadw107_area
		areadw108_area
		areadw109_area
		areadw110_area
		areadw111_area
		areadw112_area
		areadw113_area
		areadw114_area
		areadw115_area
		areadw116_area
		areadw117_area
		areadw118_area
	}
}


###########################################
#Deepwood
###########################################

west_deepwood = {
	areas = {
		areadp01_area
		areadp02_area
		areadp03_area
		areadp04_area
		areadp05_area
		areadp06_area
		areadp07_area
		areadp08_area
	}
}

east_deepwood = {
	areas = {
		areadp09_area
		areadp10_area
		areadp11_area
		areadp12_area
		areadp13_area
		areadp14_area
		areadp15_area
		areadp16_area
		areadp17_area
		areadp18_area
	}
}


# Deprecated Stuff to prevent [triggerimplementation.cpp:15371]: Unknown region used in a trigger horn_of_africa_region crashes

########################################
# EUROPE                               #
########################################

france_region = {
	areas = {

	}
}

scandinavia_region = {
	areas = {

	}
}


low_countries_region = {
	areas = {

	}

}

italy_region = {
	areas = {

	}
}


north_german_region = {
	areas = { 

	}
}




south_german_region = {
	areas = { 

	}
}

russia_region = {
	areas = {

	}
}


ural_region = {
	areas = {

	}
}

iberia_region = {
	areas = {

	}
}



british_isles_region = {
	areas = {

	}
}

baltic_region = {
	areas = {

	}
}

poland_region = {
	areas = {
		
	}
}

ruthenia_region = {
	areas = {

	}
}

crimea_region = {
	areas = {

	}
}

balkan_region = {
	areas = {

	}
}

carpathia_region = {
	areas = {

	}
}

egypt_region = {
	areas = {

	}
}

maghreb_region = {
	areas = {

	}
}

mashriq_region = {
	areas = {

	}
}

anatolia_region = {
	areas = {

	}
}

persia_region = {
	areas = {

	}
}

khorasan_region = {
	areas = {

	}
}


caucasia_region = {
	areas = {

	}
}

arabia_region = {
	areas = {

	}
}

niger_region = {
	areas = {

	}
}

guinea_region = {
	areas = {

	}
}


sahel_region = {
	areas = {

	}
}

horn_of_africa_region = {
	areas = {

	}
}

east_africa_region = {
	areas = {

	}
}

central_africa_region = {
	areas = {

	}
}

kongo_region = {
	areas = {

	}
}

central_asia_region = {
	areas = {

	}
}

south_africa_region = {
	areas = {

	}
}

west_siberia_region = {
	areas = {

	}
}

east_siberia_region = {
	areas = {

	}
}

mongolia_region = {
	areas = {

	}
}

manchuria_region = {
	areas = {

	}
}

korea_region = {
	areas = {

	}
}

tibet_region = {
	areas = {

	}
}

hindusthan_region = {
	areas = {

	}
}

bengal_region = {
	areas = {

	}
}

west_india_region = {
	areas = {
		
	}
}

deccan_region = {
	areas = {

	}
}

coromandel_region = {
	areas = {

	}
}


burma_region = {
	areas = {

	}
}

japan_region = {
	areas = {

	}
}

australia_region = {
	areas = {

	}
}


south_china_region = {
	areas = {

	}
}

xinan_region = {
	areas = {

	}
}

north_china_region = {
	
	areas = {

	}
}

brazil_region = {
	
	areas = {

	}
}

la_plata_region = {
	areas = {

	}
}


colombia_region = {
	areas = {
	
	}
}

peru_region = {
	areas = {

	}
}

upper_peru_region = {
	areas = {

	}
}

malaya_region = {	#Western Indonesia Malaya.
	areas = {

	}
}


moluccas_region = {
	areas = {

	}
}



indonesia_region = { #Eastern Indonesian/Malayan Islands.
	areas = {

	}
}


oceanea_region = {
	areas = {

	}
}

indo_china_region = {
	areas = {

	}
}

canada_region = {
	areas = {

	}
}

great_lakes_region = {
	areas = {

	}
}

northeast_america_region = {
	areas = {

	}
}

southeast_america_region =  {
	areas = {

	}
}

mississippi_region = {
	areas = {

	}
}

great_plains_region = {
	areas = {

	}
}

california_region = {
	areas = {

	}
}

cascadia_region = {
	areas = {

	}
}

hudson_bay_region = {
	areas = {

	}
}

mexico_region = {
	areas = {

	}
}

central_america_region = {
	areas = {

	}
}

carribeans_region = {
	areas = {

	}
}

#Sea Regions
baltic_sea_region = {
	areas = {

	}
}

north_atlantic_region = {
	areas = {

	}
}

american_east_coast_region = {
	areas = {

	}
}

mediterrenean_region = {
	areas = {

	}
}

caribbean_sea_region = {
	areas = {

	}
}


west_african_sea_region = {
	areas = {

	}
}

west_indian_ocean_region = {
	areas = {

	}
}

arabian_sea_region = {
	areas = {

	}
}

east_indian_ocean_region = {
	areas = {

	}
}

south_indian_ocean_region = {
	areas = {

	}
}

south_china_sea_region = {
	areas = {

	}
}

east_china_sea_region = {
	areas = {

	}
}

north_west_pacific_region = {
	areas = {

	}
}

south_west_pacific_region = {
	areas = {

	}
}

south_east_pacific_region = {
	areas = {

	}
}

north_east_pacific_region = {
	areas = {

	}
}

pacific_south_america_region = {
	areas = {

	}
}

atlantic_south_america_region = {
	areas = {

	}
}

south_atlantic_region = {
	areas = {

	}
}
