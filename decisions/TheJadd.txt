
country_decisions = {
	establish_lightbringers = {	
		potential = {
			religion = the_jadd
			NOT = { has_country_modifier = jaddari_lightbringers }
		}
		
		allow = {
			adm_power = 100
			adm_tech = 4
			temple = 1
		}
	
		effect = {
			add_adm_power = -100
			add_country_modifier = {
				name = jaddari_lightbringers
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
}