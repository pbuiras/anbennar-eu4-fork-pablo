government = monarchy
add_government_reform = dwarven_clan_reform
government_rank = 1
primary_culture = citrine_dwarf
religion = ancestor_worship
technology_group = tech_dwarven
capital = 2914

1000.1.1 = { 
	set_country_flag = mage_organization_centralized_flag
	add_country_modifier = {
		name = remnant_legacy
		duration = -1
	}
	add_country_modifier = {
		name = dwarven_administration
		duration = -1
	}
}

1442.3.17 = {
	monarch = {
		name = "Storkhumlir"
		dynasty = "az-Segh"
		birth_date = 1282.11.4
		adm = 6
		dip = 1
		mil = 6
	}
	add_ruler_personality = immortal_personality
}