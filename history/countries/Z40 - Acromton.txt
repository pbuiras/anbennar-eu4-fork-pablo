government = monarchy
add_government_reform = feudal_monarchy
government_rank = 1
primary_culture = east_damerian
religion = regent_court
technology_group = tech_cannorian
capital = 330 # Acromton
national_focus = MIL

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }