government = monarchy
add_government_reform = feudalism_reform
government_rank = 1
primary_culture = roilsardi
religion = regent_court
technology_group = tech_cannorian
capital = 32 # Saloren
national_focus = DIP

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }

1422.1.2 = { set_country_flag = is_a_county }