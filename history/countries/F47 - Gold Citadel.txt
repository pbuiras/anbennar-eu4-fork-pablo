government = republic
add_government_reform = noble_elite_reform
government_rank = 1
primary_culture = gold_dwarf
religion = ancestor_worship
technology_group = tech_dwarven
capital = 2914

1000.1.1 = { 
	set_country_flag = mage_organization_centralized_flag
	add_country_modifier = {
		name = remnant_legacy
		duration = -1
	}
}

1421.1.19 = {
	monarch = {
		name = "Urothr"
		dynasty = "of the Gate"
		birth_date = 1392.3.15
		adm = 4
		dip = 1
		mil = 6
	}
	add_ruler_personality = immortal_personality
	add_ruler_personality = strict_personality
	add_ruler_personality = cruel_personality
	set_ruler_flag = set_immortality
	add_government_reform = presidential_despot_reform
}