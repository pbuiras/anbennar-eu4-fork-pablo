


culture = cave_goblin
religion = great_dookan
trade_goods = unknown
hre = no
base_tax = 7
base_production = 8
base_manpower = 4
native_size = 84
native_ferocity = 10
native_hostileness = 10
center_of_trade = 1

add_permanent_province_modifier = {
	name = ruined_hold
	duration = -1
}