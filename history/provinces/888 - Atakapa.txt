# No previous file for Atakapa

owner = B21
controller = B21
add_core = B21
culture = green_orc
religion = great_dookan

hre = no

base_tax = 2
base_production = 2
base_manpower = 1

trade_goods = unknown
center_of_trade = 2

capital = ""

is_city = yes

native_size = 39
native_ferocity = 9
native_hostileness = 9
