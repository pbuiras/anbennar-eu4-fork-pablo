culture = green_orc
religion = great_dookan
hre = no
is_city = yes
trade_goods = unknown
base_tax = 1
base_manpower = 1
base_production = 1
native_size = 0
native_ferocity = 10
native_hostileness = 10


add_permament_province_modifier = {
	name = deepwoods_the_gladeway
	duration = -1
}
