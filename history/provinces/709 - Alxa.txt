# No previous file for Alxa
culture = green_orc
religion = great_dookan
capital = ""
trade_goods = fish

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

native_size = 18
native_ferocity = 8
native_hostileness = 9