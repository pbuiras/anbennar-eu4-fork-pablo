tribe_mechanic = {
	tribal = yes
	monarchy = yes
	basic_reform = yes # = invisible/does not take up a slot
	valid_for_nation_designer = no
	heir = yes
	queen = yes
	
	#new
	allow_migration = yes
}

steppe_horde = {
	icon = "horde_riding"
	allow_normal_conversion = no
	potential = {
		OR = {
			has_reform = steppe_horde
			culture_group = altaic
			culture_group = tartar
		}
		has_reform = steppe_horde
		NOT = { has_reform = great_mongol_state_reform }
	}
	legacy_equivalent = steppe_horde_legacy
	nomad = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	nation_designer_trigger = {
		technology_group = nomad_group
	}

	lock_level_when_selected = yes

	start_territory_to_estates = 0.5

	modifiers = {
		global_manpower_modifier = 0.2
		land_forcelimit_modifier = 0.2
		loot_amount = 0.50
		global_institution_spread = -0.15
		reinforce_cost_modifier = -0.5
		cav_to_inf_ratio = 0.25
		movement_speed = 0.2
		years_of_nationalism = -5
	}
	ai = {
		factor = 1000
	}
}

great_mongol_state_reform = {
	icon = "horde_riding"
	allow_normal_conversion = yes
	lock_level_when_selected = yes
	potential = {
		OR = {
			has_reform = great_mongol_state_reform
			have_had_reform = great_mongol_state_reform
			tag = MGE
		}
	}
	legacy_equivalent = great_mongol_state_legacy
	fixed_rank = 3
	valid_for_nation_designer = no
	nomad = yes
	modifiers = {
		horde_unity = 1
		global_manpower_modifier = 0.2
		land_forcelimit_modifier = 0.2
		loot_amount = 0.50
		reinforce_cost_modifier = -0.5
		cav_to_inf_ratio = 0.25
		movement_speed = 0.2
		years_of_nationalism = -5
	}
	conditional = {
		allow = { has_dlc = "Mandate of Heaven" }
		allow_banners = yes
	}
}

tribal_federation = {
	icon = "chieftain"
	allow_normal_conversion = yes
	potential = {
		has_reform = tribal_federation
	}

	valid_for_nation_designer = yes
	nation_designer_cost = 20
	legacy_equivalent = tribal_federation_legacy

	lock_level_when_selected = yes

	modifiers = {
		years_of_nationalism = -5
	}

	conditional = {
		allow = { has_dlc = "Cradle of Civilization" }
		government_abilities = {
			tribal_federation_mechanic
		}
	}
}

tribal_despotism = {
	icon = "tribal_council"
	allow_normal_conversion = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	modifiers = {
		core_creation = -0.1
	}
	legacy_equivalent = tribal_despotism_legacy
}

tribal_kingdom = {
	icon = "indian_crown"
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	legacy_equivalent = tribal_kingdom_legacy
	modifiers = {
		vassal_income = 0.2
	}
}

siberian_tribe = {
	icon = "shaman"
	allow_normal_conversion = no
	potential = {
		has_reform = siberian_tribe
	}
	valid_for_nation_designer = yes
	nation_designer_cost = -10
	legacy_equivalent = siberian_native_council
	native_mechanic = no
	allow_migration = yes
	fixed_rank = 1
	lock_level_when_selected = yes
	modifiers = {
		stability_cost_modifier = -0.33
		global_institution_spread = -0.2
	}
}

# Anbennar
greentide_horde = {
	icon = "shaman"
	allow_normal_conversion = no
	potential = {
		has_reform = greentide_horde
	}
	legacy_equivalent = greentide_horde_legacy
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	nation_designer_trigger = {
	
	}

	lock_level_when_selected = yes

	start_territory_to_estates = 0.5
	
	maintain_dynasty = yes

	modifiers = {	#the design ethos is 2 improved, 3 new
		#global_manpower_modifier = 0.3
		#land_forcelimit_modifier = 0.20
		land_maintenance_modifier = -0.20
		movement_speed = 0.2
		reinforce_cost_modifier = -0.1
		reform_progress_growth = -0.45
		global_institution_spread = -0.3
		diplomatic_upkeep = -2
		
		global_manpower_modifier = 0.4
		land_forcelimit_modifier = 0.3
		
		prestige_from_land = 0.5
		colonists = 1
	}
}

settled_horde = {
	icon = "shaman"
	allow_normal_conversion = no
	potential = {
		has_reform = settled_horde
	}
	legacy_equivalent = settled_horde_legacy
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	nation_designer_trigger = {
	
	}

	lock_level_when_selected = yes

	start_territory_to_estates = 0.5
	
	maintain_dynasty = yes

	modifiers = {
		global_manpower_modifier = 0.3
		land_forcelimit_modifier = 0.20
		#land_maintenance_modifier = -0.20
		movement_speed = 0.2
		reinforce_cost_modifier = -0.35
		#reform_progress_growth = -0.45
		global_institution_spread = -0.3
		diplomatic_upkeep = -2
		
		land_maintenance_modifier = -0.30
		reform_progress_growth = -0.25
		
		defensiveness = 0.25
		fort_maintenance_modifier = -0.15
		#hostile_attrition = 1
	}
}

roaming_horde = {
	icon = "shaman"
	allow_normal_conversion = no
	potential = {
		has_reform = roaming_horde
	}
	legacy_equivalent = roaming_horde_legacy
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	nation_designer_trigger = {
	
	}

	lock_level_when_selected = yes

	start_territory_to_estates = 0.5
	
	maintain_dynasty = yes

	modifiers = {
		global_manpower_modifier = 0.3
		land_forcelimit_modifier = 0.20
		#land_maintenance_modifier = -0.20
		#movement_speed = 0.2
		reinforce_cost_modifier = -0.35
		reform_progress_growth = -0.45
		global_institution_spread = -0.3
		diplomatic_upkeep = -2
		
		land_maintenance_modifier = -0.35
		movement_speed = 0.35
		
		loot_amount = 0.20
		land_attrition = -0.15
	}
}

desert_legion = {
	icon = "soldiers_2"
	valid_for_new_country = no
	allow_normal_conversion = no
	potential = {
		has_reform = desert_legion
	}
	legacy_equivalent = desert_legion_legacy
	valid_for_nation_designer = yes
	nation_designer_cost = 50
	
	modifiers = {
		global_manpower_modifier = 0.2
		land_forcelimit_modifier = 0.2
		global_institution_spread = -0.15
		reinforce_cost_modifier = -0.5
		cav_to_inf_ratio = 0.25
		movement_speed = 0.2
		global_tax_modifier = -0.1
		naval_forcelimit_modifier = -0.8
	}
	
	conditional = {
		allow = { has_dlc = "Cradle of Civilization" }
		government_abilities = {
			tribal_federation_mechanic
		}
	}
}

emerald_horde = {
	icon = "shaman"
	allow_normal_conversion = no
	potential = {
		has_reform = emerald_horde
	}
	legacy_equivalent = emerald_horde_legacy
	valid_for_nation_designer = no
	
	queen = no
	
	maintain_dynasty = yes

	modifiers = {	#the design ethos is 2 improved, 3 new
		land_morale = 0.1
		global_manpower_modifier = 0.4
		land_forcelimit_modifier = 0.3
		land_maintenance_modifier = -0.20
		movement_speed = 0.2
		reinforce_cost_modifier = -0.1
		prestige_from_land = 0.5
		monthly_war_exhaustion = -0.03			#Orcs really love wars
		
		reform_progress_growth = -0.65
		global_institution_spread = -0.45
		embracement_cost = 0.20
		diplomatic_upkeep = -2
		diplomatic_reputation = -1
		improve_relation_modifier = -0.5
	}
}

#tribal_cultural_values

martial_society_reform = {
	allow_normal_conversion = yes
	icon = "tribal_martial_society"
	modifiers = {
		global_manpower_modifier = 0.2
	}
}

civil_society_reform = {
	allow_normal_conversion = yes
	icon = "tribal_civil_society"
	modifiers = {
		global_tax_modifier = 0.05
		yearly_corruption = -0.05
	}
}

#Religion vs Secularization

religious_societies_reform = {
	icon = "tribal_religious_society"
	allow_normal_conversion = yes
	modifiers = {
		stability_cost_modifier = -0.1
		religious_unity = 0.15
	}
}

lip_service_reform = {
	icon = "tribal_lip_service"
	allow_normal_conversion = yes
	modifiers = {
		land_maintenance_modifier = -0.1
	}
}

jadd_principles_reform = {
	icon = "landscape"
	allow_normal_conversion = yes
	potential = {
		religion = the_jadd
	}
	modifiers = {
		global_missionary_strength = 0.01
		num_accepted_cultures = 1
		tolerance_own = 1
	}
	ai = {
		factor = 400
	}
}

#Modernizantion

retain_tribal_hierarchy_reform = {
	icon = "retain_tribal_hierarchy"
	allow_normal_conversion = yes
	modifiers = {
		max_states = 3
		max_absolutism = 10
	}
}

centralize_tribal_power_reform = {
	icon = "tribal_centralize_power"
	allow_normal_conversion = yes
	modifiers = {
		core_creation = -0.05
		stability_cost_modifier = -0.05
	}
}

tribe_becomes_monarchy_reform = {
	icon = "king_highlighted"
	allow_normal_conversion = yes
	effect = {
		set_country_flag = populists_in_government
		lose_reforms = 4
		if = {
			limit = {
				technology_group = nomad_group
				OR = {
					religion_group = muslim
					secondary_religion = sunni
					secondary_religion = shiite
					secondary_religion = ibadi
				}
			}
			change_technology_group = muslim
			change_unit_type = muslim
		}
		if = {
			limit = {
				technology_group = nomad_group
				religion_group = christian
			}
			change_technology_group = eastern
			change_unit_type = eastern
		}
		if = {
			limit = {
				technology_group = nomad_group
				NOT = { religion_group = muslim }
				NOT = { religion_group = christian }
				NOT = { secondary_religion = sunni }
				NOT = { secondary_religion = shiite }
				NOT = { secondary_religion = ibadi }
			}
			change_technology_group = chinese
			change_unit_type = chinese
		}
		change_government = monarchy
	}
}

tribe_becomes_republic_reform = {
	icon = "parliament_highlighted"
	allow_normal_conversion = yes
	trigger = {
		NOT = { is_lesser_in_union = yes }
	}
	effect = {
		set_country_flag = populists_in_government
		lose_reforms = 4
		if = {
			limit = {
				technology_group = nomad_group
				OR = {
					religion_group = muslim
					secondary_religion = sunni
					secondary_religion = shiite
					secondary_religion = ibadi
				}
			}
			change_technology_group = muslim
			change_unit_type = muslim
		}
		if = {
			limit = {
				technology_group = nomad_group
				religion_group = christian
			}
			change_technology_group = eastern
			change_unit_type = eastern
		}
		if = {
			limit = {
				technology_group = nomad_group
				NOT = { religion_group = muslim }
				NOT = { religion_group = christian }
				NOT = { secondary_religion = sunni }
				NOT = { secondary_religion = shiite }
				NOT = { secondary_religion = ibadi }
			}
			change_technology_group = chinese
			change_unit_type = chinese
		}
		change_government = republic
	}
}

tribe_becomes_theocracy_reform = {
	icon = "pope_highlighted"
	allow_normal_conversion = yes
	trigger = {
		NOT = { is_lesser_in_union = yes }
	}
	effect = {
		set_country_flag = populists_in_government
		lose_reforms = 4
		if = {
			limit = {
				technology_group = nomad_group
				OR = {
					religion_group = muslim
					secondary_religion = sunni
					secondary_religion = shiite
					secondary_religion = ibadi
				}
			}
			change_technology_group = muslim
			change_unit_type = muslim
		}
		else_if = {
			limit = {
				technology_group = nomad_group
				religion_group = christian
			}
			change_technology_group = eastern
			change_unit_type = eastern
		}
		else_if = {
			limit = {
				technology_group = nomad_group
				NOT = { religion_group = muslim }
				NOT = { religion_group = christian }
				NOT = { secondary_religion = sunni }
				NOT = { secondary_religion = shiite }
				NOT = { secondary_religion = ibadi }
			}
			change_technology_group = chinese
			change_unit_type = chinese
		}
		change_government = theocracy
	}
	ai = {
		factor = 0
	}
}

tribe_becomes_horde_reform = {
	icon = "horde_riding_highlighted"
	allow_normal_conversion = yes
	effect = {
		set_country_flag = populists_in_government
		add_government_reform = steppe_horde
		change_unit_type = nomad_group
		change_technology_group = nomad_group
		custom_tooltip = STEPPE_HORDE_BENEFITS
	}
	trigger = {
		OR = {
			NOT = {	has_reform = steppe_horde }
			has_reform = tribe_becomes_horde_reform
		}
		NOT = { has_reform = great_mongol_state_reform }
		NOT = { has_reform = settled_horde_reform }
		NOT = { has_reform = greentide_horde_reform }
		NOT = { has_reform = roaming_horde_reform }
		NOT = { has_reform = emerald_horde_reform }
		NOT = { has_reform = desert_legion_reform }
	}
	ai = {
		factor = 0
	}
}

#Anbennar
restructure_the_state_reform = {
	icon = "monks"
	allow_normal_conversion = yes
	potential = {
		religion = the_jadd
	}
	modifiers = {
		global_manpower_modifier = 0.1
		horde_unity = 1
		yearly_tribal_allegiance = 1
	}
	ai = {
		factor = 400
	}
}

dwarovar_warband = {
	icon = "shaman"
	allow_normal_conversion = no
	potential = {
		has_reform = dwarovar_warband
	}
	legacy_equivalent = warband_legacy
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	nation_designer_trigger = {
	
	}

	lock_level_when_selected = yes

	start_territory_to_estates = 0
	
	maintain_dynasty = yes

	modifiers = {
		global_manpower = 10
		land_forcelimit = 10
		movement_speed = 0.2
		reinforce_cost_modifier = -0.2
		reform_progress_growth = -0.55
		global_institution_spread = -0.3
		diplomatic_upkeep = -2
		global_supply_limit_modifier = 0.33
		prestige_from_land = 0.5
	}
}

dwarovar_squatter = {
	icon = "shaman"
	allow_normal_conversion = no
	potential = {
		has_reform = dwarovar_squatter
	}
	legacy_equivalent = squatter_legacy
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	nation_designer_trigger = {
	
	}

	lock_level_when_selected = yes

	start_territory_to_estates = 0
	
	maintain_dynasty = yes

	modifiers = {
		global_manpower_modifier = 0.2
		land_maintenance_modifier = -0.10
		land_attrition = -0.1
		reinforce_cost_modifier = -0.1
		reform_progress_growth = -0.45
		global_institution_spread = -0.3
		diplomatic_upkeep = -2
		
		garrison_size = 0.1
		war_taxes_cost_modifier = -0.5
	}
}
