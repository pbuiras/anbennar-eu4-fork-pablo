# Province Triggered modifiers are here.
# These are added to provinces through the add_province_triggered_modifier effect
#
# Effects are fully scriptable here.


###########################################
# Just random test modifier
###########################################
hold_has_parliament = {
	potential = {
		owner = { has_parliament = yes }
	}

	trigger = {
		has_seat_in_parliament = yes
	}
	picture = "hold_has_parliament"
	min_local_autonomy = -30
	
	#Province scope
	on_activation = {
	}
	
	on_deactivation = {
	}
}

abundant_woods = {
	potential = {
		NOT = { owner = { has_country_flag = cyranvar_expert_artisan } }
	}

	trigger = {
		has_fey_loved_culture = yes
	}
	picture = "abundant_woods"
	trade_goods_size = 1.5
	
	#Province scope
	on_activation = {
	}
	
	on_deactivation = {
	}
}

abundant_woods_cyranvar = {
	potential = {
		owner = { has_country_flag = cyranvar_expert_artisan }
	}

	trigger = {
		has_fey_loved_culture = yes
	}
	picture = "abundant_woods"
	trade_goods_size = 2
	trade_value_modifier = 0.33
	
	#Province scope
	on_activation = {
	}
	
	on_deactivation = {
	}
}

no_subterannean_race = {
	potential = {
		always = yes
	}

	trigger = {
		has_subterranean_race = no
	}
	picture = "non_subterran"
	local_tax_modifier = -0.5
	local_production_efficiency = -0.5
	
	#Province scope
	on_activation = {
	}
	
	on_deactivation = {
	}
}

efficient_irrigation = {
	potential = {
		always = yes
	}

	trigger = {
		trade_goods = grain
	}
	picture = "efficient_irrigation"
	trade_goods_size = 1
	local_development_cost = -0.1
	
	#Province scope
	on_activation = {
	}
	
	on_deactivation = {
	}
}

gold_hold = {
	potential = {
		trade_goods = gold
	}

	trigger = {
		has_subterranean_race = yes
	}
	picture = "gold_hold"
	custom_tooltip = gold_hold_tooltip
	
	#Province scope
	on_activation = {
	}
	
	on_deactivation = {
	}
}
