#All are Country Scope unless otherwise stated.

#Sets flags to use when generating advisors, must be used in immediate = { } and be followed by erase_advisor_flags_effect in after = {}
check_if_non_state_advisor_effect = {
	#Needs these arguments:
	#chance_of_primary = State religion
	#chance_of_secondary = 
	#chance_of_tertiary = Remaining Heathen Religions
	#Note: These are not actual chances, they are just weights, the script then uses them differently depending on what region you are in. There is a higher or lower chance of variety depending on where minorities where big historically.
	
	hidden_effect = { #Variety - Religious Minorities
		if = { #Europe
			limit = {
				capital_scope = {
					continent = europe
				}
			}
			random_list = {
				$chance_of_secondary$ = { set_country_flag = the_thought_advisor }
				$chance_of_primary$ = { set_country_flag = state_religion_advisor }
				$chance_of_secondary$ = { set_country_flag = corinite_or_elven_forebears_advisor }
				$chance_of_secondary$ = { set_country_flag = reformed_or_orthodox_advisor }
				$chance_of_tertiary$ = { set_country_flag = heathen_advisor }
			}
		}

		else_if = {
			limit = {
				capital_scope = {
					continent = new_world #RNW
				}
			}
			random_list = { #East Asia has highest chance of non state.
				$chance_of_primary$ = { set_country_flag = state_religion_advisor }
				$chance_of_tertiary$ = { set_country_flag = great_dookan_advisor }
				$chance_of_primary$ = { set_country_flag = fey_court_advisor }
				$chance_of_secondary$ = { set_country_flag = mother_akan_advisor }
				$chance_of_secondary$ = { set_country_flag = mayan_advisor }
				$chance_of_secondary$ = { set_country_flag = inca_advisor }
				$chance_of_secondary$ = { set_country_flag = nahuatl_advisor }
			}
		}

		else = {
			set_country_flag = state_religion_advisor
		}
	}
}

erase_advisor_flags_effect = { #Goes into the after = {} effect after check_if_non_state_advisor_effect
	trigger_switch = {
		on_trigger = has_country_flag
		state_religion_advisor = { clr_country_flag = state_religion_advisor }
		the_thought_advisor = { clr_country_flag = the_thought_advisor }
		corinite_or_elven_forebears_advisor = { clr_country_flag = corinite_or_elven_forebears_advisor }
		reformed_or_orthodox_advisor = { clr_country_flag = reformed_or_orthodox_advisor }
		heathen_advisor = { clr_country_flag = heathen_advisor }
		khetist_advisor = { clr_country_flag = khetist_advisor }
		sikh_advisor = { clr_country_flag = sikh_advisor }
		confucian_advisor = { clr_country_flag = confucian_advisor }
		shinto_advisor = { clr_country_flag = shinto_advisor }
		theravada_advisor = { clr_country_flag = theravada_advisor }
		mahayana_advisor = { clr_country_flag = mahayana_advisor }
		vajrayana_advisor = { clr_country_flag = vajrayana_advisor }
		regent_court_advisor = { clr_country_flag = regent_court_advisor }
		skaldhyrric_faith_advisor = { clr_country_flag = skaldhyrric_faith_advisor }
		ibadi_advisor = { clr_country_flag = ibadi_advisor }
		shia_advisor = { clr_country_flag = shia_advisor }
		bulwari_sun_cult_advisor = { clr_country_flag = bulwari_sun_cult_advisor }
		elven_forebears_advisor = { clr_country_flag = elven_forebears_advisor }
		great_dookan_advisor = { clr_country_flag = great_dookan_advisor }
		mother_akan_advisor = { clr_country_flag = mother_akan_advisor }
		fey_court_advisor = { clr_country_flag = fey_court_advisor }
		inca_advisor = { clr_country_flag = inca_advisor }
		nahuatl_advisor = { clr_country_flag = nahuatl_advisor }
		mayan_advisor = { clr_country_flag = mayan_advisor }
	}
}

internal_advisor_generation_effect = { #Only used within generate_advisor_of_type_and_semi_random_religion_effect
#Requires check_if_non_state_advisor_effect in immediate as that gives the arguments to generate_advisor_of_type_and_semi_random_religion_effect that then passes it on to this.
#$advisor_type$ = Advisor type if state religion.
#$advisor_type_if_not_state$ = Advisor type if non-state religion.
#$skill$ = Skill Level
#$discount$ = yes/no for discount
	trigger_switch = {
		on_trigger = has_country_flag #Flags set by check_if_non_state_advisor_effect
		state_religion_advisor = {
			define_advisor = {
				type = $advisor_type$
				skill = $skill$
				discount = $discount$
			}
		}
		the_thought_advisor = {
			define_advisor = {
				type = $advisor_type_if_not_state$
				skill = $skill$
				discount = $discount$
				religion = the_thought
			}
		}
		corinite_or_elven_forebears_advisor = {
			if = {
				limit = {
					OR = {
						religion = regent_court
						religion = reformed
					}
					is_religion_enabled = corinite
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = corinite
				}
			}
			else_if = {
				limit = {
					religion = regent_court
					NOT = { is_religion_enabled = corinite }
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = orthodox
				}
			}
			else_if = {
				limit = {
					religion = corinite
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = regent_court
				}
			}
			else_if = {
				limit = {
					religion = orthodox
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = elven_forebears
				}
			}
			else_if = {
				limit = {
					religion = elven_forebears
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = the_thought
				}
			}
			else = {
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
				}
			}
		}
		reformed_or_orthodox_advisor = {
			if = {
				limit = {
					OR = {
						religion = regent_court
						religion = corinite
					}
					is_religion_enabled = reformed
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = reformed
				}
			}
			else_if = {
				limit = {
					religion = regent_court
					NOT = { is_religion_enabled = reformed }
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = orthodox
				}
			}
			else_if = {
				limit = {
					religion = reformed
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = regent_court
				}
			}
			else_if = {
				limit = {
					religion = orthodox
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = elven_forebears
				}
			}
			else_if = {
				limit = {
					religion = elven_forebears
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = orthodox
				}
			}
			else = {
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
				}
			}
		}
		heathen_advisor = {
			if = {
				limit = {
					religion_group = cannorian
					capital_scope = { continent = europe }
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = the_thought
				}
			}
			else_if = {
				limit = {
					religion_group = cannorian
					capital_scope = { continent = asia }
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = bulwari_sun_cult
				}
			}
			else_if = {
				limit = {
					religion_group = cannorian
					capital_scope = { continent = africa }
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = bulwari_sun_cult
				}
			}
			else_if = {
				limit = {
					religion_group = bulwari
					continent = europe
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = regent_court
				}
			}
			else_if = {
				limit = {
					religion_group = bulwari
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = elven_forebears
				}
			}
			else_if = {
				limit = {
					religion_group = eastern #Most likely won't come here but could.
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = mother_akan
				}
			}
			else_if = {
				limit = {
					religion_group = dharmic #Most likely won't come here but could.
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = mother_akan
				}
			}
			else_if = {
				limit = {
					religion_group = dharmic #Most likely won't come here but could.
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = mother_akan
				}
			}
			else = {
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
				}
			}
		}
		heathen_advisor = {
			if = {
				limit = { religion_group = cannorian }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = bulwari_sun_cult
				}
			}
			else_if = {
				limit = {
					religion_group = bulwari
					capital_scope = {
						OR = {
							continent = europe
						}
					}
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = regent_court
				}
			}
			else_if = {
				limit = { religion_group = bulwari }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = khetist
				}
			}
			else_if = {
				limit = { religion_group = dharmic }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
					religion = shiite
					culture = persian
				}
			}
			else = {
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
				}
			}
		}
		khetist_advisor = {
			if = {
				limit = { NOT = { religion = khetist } }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = khetist
					discount = $discount$
				}
			}
			else = {
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = skaldhyrric_faith
					discount = $discount$
				}
			}
		}
		sikh_advisor = {
			if = {
				limit = { religion = sikhism }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = skaldhyrric_faith
					discount = $discount$
				}
			}
			else_if = {
				limit = { is_religion_enabled = sikhism }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = sikhism
					discount = $discount$
				}
			}
			else_if = {
				limit = { religion = skaldhyrric_faith }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = khetist
					discount = $discount$
				}
			}
			else_if = {
				limit = { religion_group = bulwari }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = skaldhyrric_faith
					discount = $discount$
				}
			}
			else = {
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
				}
			}
		}
		confucian_advisor = {
			define_advisor = {
				type = $advisor_type_if_not_state$
				skill = $skill$
				religion = confucianism
				discount = $discount$
			}
		}
		shinto_advisor = {
			define_advisor = {
				type = $advisor_type_if_not_state$
				skill = $skill$
				religion = shinto
				discount = $discount$
			}
		}
		theravada_advisor = {
			define_advisor = {
				type = $advisor_type_if_not_state$
				skill = $skill$
				religion = buddhism
				discount = $discount$
			}
		}
		mahayana_advisor = {
			define_advisor = {
				type = $advisor_type_if_not_state$
				skill = $skill$
				religion = mahayana
				discount = $discount$
			}
		}
		vajrayana_advisor = {
			define_advisor = {
				type = $advisor_type_if_not_state$
				skill = $skill$
				religion = vajrayana
				discount = $discount$
			}
		}
		regent_court_advisor = {
			if = {
				limit = {
					any_province = {
						religion = regent_court
						has_discovered = ROOT
					}
				}
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = regent_court
					discount = $discount$
				}
			}
			else = {
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					discount = $discount$
				}
			}
		}
		skaldhyrric_faith_advisor = {
			if = {
				limit = { NOT = { religion = skaldhyrric_faith } }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = skaldhyrric_faith
					discount = $discount$
				}
			}
			else = {
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = bulwari_sun_cult
					discount = $discount$
				}
			}
		}
		ibadi_advisor = {
			if = {
				limit = { NOT = { religion = ibadi } }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = ibadi
					discount = $discount$
				}
			}
			else = {
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = shiite
					discount = $discount$
				}
			}
		}
		shia_advisor = {
			if = {
				limit = { NOT = { religion = shiite } }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = shiite
					discount = $discount$
				}
			}
			else = {
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = ibadi
					discount = $discount$
				}
			}
		}
		bulwari_sun_cult_advisor = {
			if = {
				limit = { NOT = { religion = bulwari_sun_cult } }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = bulwari_sun_cult
					discount = $discount$
				}
			}
			else = {
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = shiite
					discount = $discount$
				}
			}
		}
		elven_forebears_advisor = {
			if = {
				limit = { NOT = { religion = elven_forebears } }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = elven_forebears
					discount = $discount$
				}
			}
			else = {
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = the_thought
					discount = $discount$
				}
			}
		}
		great_dookan_advisor = {
			if = {
				limit = { NOT = { religion = great_dookan_pagan_reformed } }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = great_dookan_pagan_reformed
					discount = $discount$
				}
			}
			else = {
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = fey_court
					discount = $discount$
				}
			}
		}
		mother_akan_advisor = {
			if = {
				limit = { NOT = { religion = mother_akan } }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = mother_akan
					discount = $discount$
				}
			}
			else = {
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = fey_court
					discount = $discount$
				}
			}
		}
		fey_court_advisor = {
			if = {
				limit = { NOT = { religion = fey_court } }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = fey_court
					discount = $discount$
				}
			}
			else = {
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = mother_akan
					discount = $discount$
				}
			}
		}
		mayan_advisor = {
			if = {
				limit = { NOT = { religion = mesoamerican_religion } }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = mesoamerican_religion
					discount = $discount$
				}
			}
			else = {
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = nahuatl
					discount = $discount$
				}
			}
		}
		nahuatl_advisor = {
			if = {
				limit = { NOT = { religion = nahuatl } }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = nahuatl
					discount = $discount$
				}
			}
			else = {
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = mesoamerican_religion
					discount = $discount$
				}
			}
		}
		inca_advisor = {
			if = {
				limit = { NOT = { religion = inti } }
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = inti
					discount = $discount$
				}
			}
			else = {
				define_advisor = {
					type = $advisor_type_if_not_state$
					skill = $skill$
					religion = mother_akan
					discount = $discount$
				}
			}
		}
	}
}

generate_advisor_of_type_and_semi_random_religion_effect = {
#Actual work is done by internal_advisor_generation_effect above but it goes through this first.
#Requires check_if_non_state_advisor_effect in immediate.
#$advisor_type$ = Advisor type if state religion.
#$advisor_type_if_not_state$ = Advisor type if non-state religion.
#$scaled_skill$ = yes/no
#$skill$ = Skill Level
#$discount$ = yes/no for discount
	
	[[scaled_skill]
		if = {
			limit = {
				NOT = { monthly_income = 15 }
			}
			internal_advisor_generation_effect = {
				advisor_type = $advisor_type$
				advisor_type_if_not_state = $advisor_type_if_not_state$
				skill = 1
				discount = $discount$
			}
		}
		else_if = {
			limit = {
				NOT = { monthly_income = 25 }
			}
			internal_advisor_generation_effect = {
				advisor_type = $advisor_type$
				advisor_type_if_not_state = $advisor_type_if_not_state$
				skill = 2
				discount = $discount$
			}
		}
		else = {
			internal_advisor_generation_effect = {
				advisor_type = $advisor_type$
				advisor_type_if_not_state = $advisor_type_if_not_state$
				skill = 3
				discount = $discount$
			}
		}
	]
	[[skill]
		internal_advisor_generation_effect = {
			advisor_type = $advisor_type$
			advisor_type_if_not_state = $advisor_type_if_not_state$
			skill = $skill$
			discount = $discount$
		}
	]
}


remove_advisor_adm_effect = {
	if = {
		limit = {
			advisor = inquisitor
		}
		remove_advisor = inquisitor
	}
	if = {
		limit = {
			advisor = natural_scientist
		}
		remove_advisor = natural_scientist
	}
	if = {
		limit = {
			advisor = master_of_mint
		}
		remove_advisor = master_of_mint
	}
	if = {
		limit = {
			advisor = theologian
		}
		remove_advisor = theologian
	}
	if = { 
		limit = {
			advisor = artist
		}
		remove_advisor = artist
	}
	if = {
		limit = {
			advisor = philosopher
		}
		remove_advisor = philosopher
	}
	if = {
		limit = { 
			advisor = treasurer
		}
		remove_advisor = treasurer
	}
}

